import { fork, all } from 'redux-saga/effects';
import { watchGetEmployeeSignaturesData } from '../redux/signature';
import { watchGetMySignaturesData } from '../redux/mySignature';
import { watchGetUserData } from '../redux/user';
import { watchGetContractDetailsData, watchPutSignatureData } from '../redux/contractDetails';
import { watchGetActivesData } from '../redux/active';

export default function* rootSaga(): Generator<all, void, fork> {
  yield all([
    fork(watchGetEmployeeSignaturesData),
    fork(watchGetUserData),
    fork(watchGetMySignaturesData),
    fork(watchGetContractDetailsData),
    fork(watchPutSignatureData),
    fork(watchGetActivesData),
  ]);
}
