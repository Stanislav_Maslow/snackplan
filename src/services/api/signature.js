import { AsyncStorage } from 'react-native';
import ApiUtils from './api-utils';

export const SignatureApi = {
  async getEmployeeSignature() {
    const token = await AsyncStorage.getItem('token');
    console.log(1);
    const url = `${ApiUtils.getBaseUrl()}/payplan/contracts/signature?_json=1`;
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(url, options)
      .then(response => ApiUtils.checkStatus(response, options))
      .then(async (response) => {
        let parsed = null;
        parsed = await response.json();

        return parsed.contracts;
      })
      .catch((err) => {
        console.log('err', err);
      });
  },
  async getReviewPending() {
    console.log(2);
    const token = await AsyncStorage.getItem('token');
    const url = `${ApiUtils.getBaseUrl()}/payplan/contracts/pending?_json=1`;
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(url, options)
      .then(response => ApiUtils.checkStatus(response, options))
      .then(async (response) => {
        let parsed = null;
        parsed = await response.json();
        return parsed.contracts;
      })
      .catch((err) => {
        console.log('err', err.response);
      });
  },

  async getActive() {
    console.log(5);
    const token = await AsyncStorage.getItem('token');
    const url = `${ApiUtils.getBaseUrl()}/payplan/contracts/active?_json=1`;
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(url, options)
      .then(response => ApiUtils.checkStatus(response, options))
      .then(async (response) => {
        let parsed = null;
        parsed = await response.json();
        return parsed.contracts;
      })
      .catch((err) => {
        console.log('err', err.response);
      });
  },

  async getForApproval() {
    console.log(3);
    const token = await AsyncStorage.getItem('token');
    const url = `${ApiUtils.getBaseUrl()}/payplan/contracts/approval?_json=1`;
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(url, options)
      .then(response => ApiUtils.checkStatus(response, options))
      .then(async (response) => {
        let parsed = null;
        parsed = await response.json();
        // console.log('parced approval', parsed);
        return parsed.contracts;
      })
      .catch((err) => {
        console.log('err', err);
      });
  },
  async getContractDetails(id) {
    const token = await AsyncStorage.getItem('token');
    console.log('>>>>>>>>>>getContractsDetails called', id);
    const url = `${ApiUtils.getBaseUrl()}/payplan/contract/${id}?_json`;
    const options = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(url, options)
      .then(response => ApiUtils.checkStatus(response, options))
      .then(async (response) => {
        let parsed = null;
        parsed = await response.json();

        return parsed;
      })
      .catch((err) => {
        console.log('err', err);
      });
  },
  async putSignature(data) {
    const token = await AsyncStorage.getItem('token');
    console.log('>>>>>>>>>>PutSignatureDetails called', data);
    const url = `${ApiUtils.getBaseUrl()}/payplan/contract/${data.contract_id}/setstatus/${
      data.action_value
    }`;
    const bodyData = { Contract: data };

    const options = {
      method: 'POST',
      body: JSON.stringify(bodyData),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(url, options)
      .then(response => ApiUtils.checkStatus(response, options))
      .then(async (response) => {
        let parsed = null;
        parsed = await response.json();
        console.log('----parced-------', parsed);
        return parsed;
      })
      .catch((err) => {
        console.log('err', err);
      });
  },
};
