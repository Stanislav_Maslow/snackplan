import { AsyncStorage } from 'react-native';
import NavigationService from '../navigation';

const ApiUtils = {
  checkStatus: (response, options) => {
    if (response.ok) {
      return response;
    }
    if (response.status === 401) {
      AsyncStorage.removeItem('token', () => {
        NavigationService.navigate('Login');
      });
    }
    if (response.status === 500) {
      // console.log('req', JSON.stringify(options || {}))
      // console.log('res', response)
    }
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  },
  getBaseUrl: () => 'https://payplan.dev.genevamedia.com',
};
export { ApiUtils as default };
