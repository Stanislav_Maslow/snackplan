import ApiUtils from './api-utils'
import {AsyncStorage} from 'react-native';
import {Toast} from "native-base";
import { NavigationActions } from 'react-navigation';
const path = '/users';
const adminPath = '/admin';

export const UserApi = {
    login: function (credentials) {
        const url = `${ApiUtils.getBaseUrl()}/authenticate?_json`;
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                User: credentials
            })
        }
        return fetch(url, options)
            .then(response => ApiUtils.checkStatus(response, options))
            .then(response => {
                let parsed = null;
                parsed = response.json()
                return parsed;
            })
    },

    get: async function () {
        const token = await AsyncStorage.getItem('token');
        const url = `${ApiUtils.getBaseUrl()}/user/account/?_json=1`;
        console.log(4)
        const options =  {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        };

        return fetch(url, options)
            .then(response => ApiUtils.checkStatus(response, options))
            .then(async (response) => {
                let parsed = null;
                parsed = await response.json()
                return parsed;
            })
            .catch(async err => {
                return err.response
            })
    },

    update: async (userId, userData) => {
        const token = await AsyncStorage.getItem('token');
        const url = `${ApiUtils.getBaseUrl()}/admin/user/${userId}`;
        return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                User: userData
            })
        })
            .then(response => {
                let parsed = null;
                parsed = response.json()
                return parsed;
            })    
            .catch(err => console.log('err', err))
    },

    changePassword: async (user) => {
        const token = await AsyncStorage.getItem('token');
        const url = `${ApiUtils.getBaseUrl()}/user/changePassword`;
        return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                User: user
            })
        })
            .then(response => {
                let parsed = null;
                parsed = response.json()
                return parsed;
            })    
            .catch(err => console.log('err', err))
    }
};