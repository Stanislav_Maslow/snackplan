const CHANGE_DASHBOARD_TAB = 'CHANGE_DASHBOARD_TAB';

// actions
export const changeTab = payload => ({
  type: CHANGE_DASHBOARD_TAB,
  payload,
});

// reducers
const initialState = {
  tabName: 'latest',
};

export function dashboardTabReducer(state = initialState, action) {
  switch (action.type) {
  case CHANGE_DASHBOARD_TAB:
    return {
      ...state,
      tabName: action.payload,
    };
  default:
    return state;
  }
}
