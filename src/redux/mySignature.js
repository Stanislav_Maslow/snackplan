import { call, put, takeLatest } from 'redux-saga/effects';

import { SignatureApi } from '../services/api/signature';

const API_MY_SIGNATURE_REQUEST = 'API_MY_SIGNATURE_REQUEST';
const API_MY_SIGNATURE_SUCCESS = 'API_MY_SIGNATURE_SUCCESS';
const API_MY_SIGNATURE_ERROR = 'API_MY_SIGNATURE_ERROR';

export const getMySignatureRequest = () => ({
  type: API_MY_SIGNATURE_REQUEST,
});

const getMySignatureSuccess = payload => ({
  type: API_MY_SIGNATURE_SUCCESS,
  payload,
});

const getMySignatureError = error => ({
  type: API_MY_SIGNATURE_ERROR,
  error,
});

const initialState = {
  mySignature: [],
  loading: false,
  error: false,
};

export function mySignatureReducer(state = initialState, action) {
  switch (action.type) {
  case API_MY_SIGNATURE_REQUEST:
    return {
      ...state,
      loading: true,
      error: false,
    };
  case API_MY_SIGNATURE_SUCCESS:
    return {
      ...state,
      mySignature: action.payload,
      loading: false,
      error: false,
    };
  case API_MY_SIGNATURE_ERROR:
    return {
      ...state,
      loading: false,
      error: true,
    };
  default:
    return state;
  }
}

function* getMySignaturesData() {
  try {
    const mySignature = [];
    const reviewPending = yield call(SignatureApi.getReviewPending);
    const forApproval = yield call(SignatureApi.getForApproval);
    if (reviewPending) {
      reviewPending.map(signature => mySignature.push(signature));
    }
    forApproval.map(signature => mySignature.push(signature));
    yield put(getMySignatureSuccess(mySignature));
  } catch (error) {
    yield put(getMySignatureError(error));
  }
}

export function* watchGetMySignaturesData() {
  yield takeLatest(API_MY_SIGNATURE_REQUEST, getMySignaturesData);
}
