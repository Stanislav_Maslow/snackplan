import { call, put, takeLatest } from 'redux-saga/effects';

import { SignatureApi } from '../services/api/signature';

const API_EMPLOYEE_SIGNATURE_REQUEST = 'API_EMPLOYEE_SIGNATURE_REQUEST';
const API_EMPLOYEE_SIGNATURE_SUCCESS = 'API_EMPLOYEE_SIGNATURE_SUCCESS';
const API_EMPLOYEE_SIGNATURE_ERROR = 'API_EMPLOYEE_SIGNATURE_ERROR';

// actions
export const getEmployeeSignatureRequest = () => ({
  type: API_EMPLOYEE_SIGNATURE_REQUEST,
});

const getEmployeeSignatureSuccess = payload => ({
  type: API_EMPLOYEE_SIGNATURE_SUCCESS,
  payload,
});

const getEmployeeSignatureError = error => ({
  type: API_EMPLOYEE_SIGNATURE_ERROR,
  error,
});

// reducers
const initialState = {
  employeeSignature: [],
  loading: false,
  error: false,
};

export function employeeSignatureReducer(state = initialState, action) {
  switch (action.type) {
  case API_EMPLOYEE_SIGNATURE_REQUEST:
    return {
      ...state,
      loading: true,
      error: false,
    };
  case API_EMPLOYEE_SIGNATURE_SUCCESS:
    return {
      ...state,
      employeeSignature: action.payload,
      loading: false,
      error: false,
    };
  case API_EMPLOYEE_SIGNATURE_ERROR:
    return {
      ...state,
      loading: false,
      error: true,
    };
  default:
    return state;
  }
}

// saga
function* getEmployeeSignaturesData() {
  try {
    const employeeSignature = yield call(SignatureApi.getEmployeeSignature);
    yield put(getEmployeeSignatureSuccess(employeeSignature));
  } catch (error) {
    yield put(getEmployeeSignatureError(error));
  }
}

export function* watchGetEmployeeSignaturesData() {
  yield takeLatest(API_EMPLOYEE_SIGNATURE_REQUEST, getEmployeeSignaturesData);
}
