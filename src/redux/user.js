import { call, put, takeLatest } from 'redux-saga/effects'
// import store from './../store'

import { UserApi } from './../services/api/user'
import { NavigationActions } from 'react-navigation';
import {AsyncStorage} from 'react-native';

const API_USER_DATA_REQUEST = 'API_USER_DATA_REQUEST'
const API_USER_DATA_SUCCESS = 'API_USER_DATA_SUCCESS'
const API_USER_DATA_ERROR = 'API_USER_DATA_ERROR'

// actions
export const getUserDataRequest = () => ({
  type: API_USER_DATA_REQUEST
})

const getUserDataSuccess = payload => ({
  type: API_USER_DATA_SUCCESS,
  payload
})

const getUserDataError = error => ({
  type: API_USER_DATA_ERROR,
  error
})


// reducers
const initialState = {
  userData: {},
  loading: false,
  error: false
}

export function userDataReducer(state = initialState, action) {
  switch (action.type) {
    case API_USER_DATA_REQUEST:
      return {
        ...state,
        loading: true,
        error: false
      }
    case API_USER_DATA_SUCCESS:
      return {
        ...state,
        userData: action.payload,
        loading: false,
        error: false
      }
    case API_USER_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: true
      }
    default:
      return state
  }
}

// saga
function* getUserData() {
  try {
    const userData = yield call(UserApi.get)
    yield put(getUserDataSuccess(userData))
  } catch (error) {
    yield put(getUserDataError(error))
  }
}

export function* watchGetUserData() {
  yield takeLatest(API_USER_DATA_REQUEST, getUserData)
}
