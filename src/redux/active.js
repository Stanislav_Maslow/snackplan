import { call, put, takeLatest } from 'redux-saga/effects';

import { SignatureApi } from '../services/api/signature';

const API_ACTIVE_REQUEST = 'API_ACTIVE_REQUEST';
const API_ACTIVE_SUCCESS = 'API_ACTIVE_SUCCESS';
const API_ACTIVE_ERROR = 'API_ACTIVE_ERROR';

// actions
export const getActiveRequest = () => ({
  type: API_ACTIVE_REQUEST,
});

const getActiveSuccess = payload => ({
  type: API_ACTIVE_SUCCESS,
  payload,
});

const getActiveError = error => ({
  type: API_ACTIVE_ERROR,
  error,
});

// reducers
const initialState = {
  active: [],
  loading: false,
  error: false,
};

export function activeReducer(state = initialState, action) {
  switch (action.type) {
  case API_ACTIVE_REQUEST:
    return {
      ...state,
      loading: true,
      error: false,
    };
  case API_ACTIVE_SUCCESS:
    return {
      ...state,
      active: action.payload,
      loading: false,
      error: false,
    };
  case API_ACTIVE_ERROR:
    return {
      ...state,
      loading: false,
      error: true,
    };
  default:
    return state;
  }
}

// saga
function* getActivesData() {
  try {
    const active = yield call(SignatureApi.getActive);
    yield put(getActiveSuccess(active));
  } catch (error) {
    yield put(getActiveError(error));
  }
}

export function* watchGetActivesData() {
  yield takeLatest(API_ACTIVE_REQUEST, getActivesData);
}
