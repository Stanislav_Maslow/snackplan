import { call, put, takeLatest } from 'redux-saga/effects';

import { SignatureApi } from '../services/api/signature';

const API_CONTRACT_DETAILS_REQUEST = 'API_CONTRACT_DETAILS_REQUEST';
const API_CONTRACT_DETAILS_SUCCESS = 'API_CONTRACT_DETAILS_SUCCESS';
const API_CONTRACT_DETAILS_ERROR = 'API_CONTRACT_DETAILS_ERROR';

const API_PUT_SIGNATURE_REQUEST = 'API_PUT_SIGNATURE_REQUEST';
const API_PUT_SIGNATURE_SUCCESS = 'API_PUT_SIGNATURE_SUCCESS';
const API_PUT_SIGNATURE_ERROR = 'API_PUT_SIGNATURE_ERROR';

// actions
export const getContractDetailsRequest = (id) => {
  console.log('id in action---------------', id);
  return {
    type: API_CONTRACT_DETAILS_REQUEST,
    id,
  };
};

const getContractDetailsSuccess = payload => ({
  type: API_CONTRACT_DETAILS_SUCCESS,
  payload,
});

const getContractDetailsError = error => ({
  type: API_CONTRACT_DETAILS_ERROR,
  error,
});

export const putSignatureRequest = data => ({
  type: API_PUT_SIGNATURE_REQUEST,
  data,
});

const putSignatureSuccess = payload => ({
  type: API_PUT_SIGNATURE_SUCCESS,
  payload,
});

const putSignatureError = error => ({
  type: API_PUT_SIGNATURE_ERROR,
  error,
});

// reducers
const initialState = {
  contractDetails: [],
  signaturePutStatus: {},
  loading: false,
  error: false,
};

export function contractDetailsReducer(state = initialState, action) {
  switch (action.type) {
  case API_CONTRACT_DETAILS_REQUEST:
    return {
      ...state,
      signaturePutStatus: { status: 0 },
      loading: true,
      error: false,
    };
  case API_CONTRACT_DETAILS_SUCCESS:
    return {
      ...state,
      contractDetails: action.payload,
      loading: false,
      error: false,
    };
  case API_CONTRACT_DETAILS_ERROR:
    return {
      ...state,
      loading: false,
      error: true,
    };
  case API_PUT_SIGNATURE_REQUEST:
    return {
      ...state,
      loading: true,
      error: false,
    };
  case API_PUT_SIGNATURE_SUCCESS:
    return {
      ...state,
      signaturePutStatus: action.payload,
      loading: false,
      error: false,
    };
  case API_PUT_SIGNATURE_ERROR:
    return {
      ...state,
      loading: false,
      error: true,
    };
  default:
    return state;
  }
}

// saga
function* getContractDetailsData(action) {
  console.log('----saga-------', action);
  try {
    const employeeSignature = yield call(SignatureApi.getContractDetails, action.id);
    yield put(getContractDetailsSuccess(employeeSignature));
  } catch (error) {
    yield put(getContractDetailsError(error));
  }
}

export function* watchGetContractDetailsData() {
  yield takeLatest(API_CONTRACT_DETAILS_REQUEST, getContractDetailsData);
}

function* putSignatureData(action) {
  try {
    const employeeSignature = yield call(SignatureApi.putSignature, action.data);
    yield put(putSignatureSuccess(employeeSignature));
  } catch (error) {
    yield put(putSignatureError(error));
  }
}

export function* watchPutSignatureData() {
  yield takeLatest(API_PUT_SIGNATURE_REQUEST, putSignatureData);
}
