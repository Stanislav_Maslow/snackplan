import React from "react";
import {Root} from "native-base";
import {StackNavigator, DrawerNavigator} from "react-navigation";
import NavigationService from './services/navigation';

import Login from './screens/login'
import ForgotPassword from './screens/forgot-password'
import Dashboard from './screens/dashboard'
import Settings from './screens/settings'
import SideBar from './screens/sidebar'
import Signature from './screens/signature'

const Drawer = DrawerNavigator(
    {
        Login: {screen: Login},
        ForgotPassword: {screen: ForgotPassword},
        Dashboard: {screen: Dashboard},
        Settings: {screen: Settings},
        Signature: {screen: Signature}
    },
    {
        initialRouteName: "Login",
        contentOptions: {
            activeTintColor: "#e91e63"
        },
        contentComponent: props => <SideBar {...props} />
    }
);

const AppNavigator = StackNavigator(
    {
        Drawer: {screen: Drawer},

    },
    {
        initialRouteName: "Drawer",
        headerMode: "none"
    }
);

export default () =>
    <Root>
        <AppNavigator ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>
    </Root>;