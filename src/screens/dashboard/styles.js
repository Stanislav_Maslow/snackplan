import React, {Dimensions, Platform} from 'react-native';

export default {
    mine: {
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        flex: 1
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    actionsHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 40
    },
    tabsSection: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    image: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 20,
        alignSelf: 'center',
    },
    container: {
        paddingTop: 50,
    },
    card: {
        marginTop: 0,
        backgroundColor: 'rgb(251,252,253)'
    },
    tableHeader: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 54,
        borderColor: 'rgb(235,235,235)',
        borderBottomWidth: 1,
    },
    tableRow: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 71,
        borderColor: 'rgb(235,235,235)',
        borderBottomWidth: 1,
        backgroundColor: 'rgb(251,252,253)'
    },
    tableRowActive: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        height: 71,
        borderColor: 'rgb(235,235,235)',
        borderBottomWidth: 1,
        backgroundColor: 'rgb(255,255,255)'
    },
    headerItem: {
        color: 'rgb(127,133,140)',
        fontSize: 13,
        letterSpacing: -0.1
    },
    searchBar: {
        flex: 1,
        backgroundColor: '#fff',
        height: 50,
        paddingRight: 10,
        paddingLeft: 10,
    },
    menu: {
        backgroundColor: '#fff',
        height: 50,
        width: 50,
        marginRight: 25,
        paddingRight: 0,
        alignItems: 'center'
    },
    menuIconBtn: {
        flex: 1,
        borderBottomWidth: 0, 
        backgroundColor: '#fff'
    },
    menuIcon: {
        color: 'rgb(0,119,255)', 
        paddingRight: 0, 
        paddingLeft: 0
    },
    iconSearch: {
        color: 'rgb(0,119,255)'
    },
    tab: {
        padding: 20,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        width: 209,
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'white',
        marginRight: 10,
        marginLeft: 2
    },
    badge: {
        width: 25, 
        height: 25,
        borderRadius: 100, 
        backgroundColor: 'rgb(0,119,255)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    badgeText: {
        color: '#fff', 
        textAlign: 'center',
        fontSize: 13
    },
    tabText: {
        color: "black"
    },
    tabActive: {
        padding: 20,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        width: 209,
        height: 60,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgb(0,119,255)',
        marginRight: 10,
        marginLeft: 2
    },
    badgeActive: {
        width: 25, 
        height: 25,
        borderRadius: 100, 
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    badgeTextActive: {
        color: 'rgb(0,119,255)', 
        textAlign: 'center',
        fontSize: 13
    },
    tabTextActive: {
        color: 'white'
    },
    tableTitle: {
        fontSize: 28, 
        color: 'rgb(71,79,88)', 
        marginTop: 34, 
        marginBottom: 34
    },
    tableContainer: {
        marginLeft: 20, 
        marginRight: 20
    },
    latestTab: {
        alignItems: 'center', 
        justifyContent: 'center', 
        flexDirection: 'row', 
        height: 60, 
        width: 120, 
        borderBottomColor: 'rgb(0,119,255)', 
        marginRight: 2
    },
    latestIcon: {
        marginRight: 10, 
        fontSize: 18
    }
};
