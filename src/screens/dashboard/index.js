import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View, StatusBar, TouchableOpacity, ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';

import {
  Container, Icon, Text, Card, Item, Input, Button,
} from 'native-base';

import styles from './styles';

import { getEmployeeSignatureRequest } from '../../redux/signature';
import { getMySignatureRequest } from '../../redux/mySignature';
import { getActiveRequest } from '../../redux/active';
import { changeTab } from '../../redux/dashboardTabs';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: null,
      activeTab: 'employee',
    };
  }

  static propTypes = {
    getEmployeeSignatureRequest: PropTypes.func,
    getMySignatureRequest: PropTypes.func,
    getActiveRequest: PropTypes.func,
  };

  static defaultProps = {
    getEmployeeSignatureRequest: () => {},
    getMySignatureRequest: () => {},
    getActiveRequest: () => {},
  };
  componentDidMount() {
    console.log('dashboard------');
    this.props.getEmployeeSignatureRequest();
    this.props.getMySignatureRequest();
    this.props.getActiveRequest();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      activeTab: nextProps.tab,
    });
  }

  // setActive(id) {
  //   this.setState({ active: id });
  // }

  selectSignature(id) {
    this.props.navigation.navigate('Signature', { itemId: id });
  }

  renderHeader() {
    return (
      <View style={styles.tableHeader} key="header">
        <View style={{ flex: 3, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Contact #
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Emploee name
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Position
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Department
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Created On
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Who reviewed
          </Text>
        </View>
        <View style={{ flex: 1, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Age
          </Text>
        </View>
        <View style={{ flex: 1, alignSelf: 'center' }}>
          <Text style={styles.headerItem}>
Time Stamp
          </Text>
        </View>
      </View>
    );
  }

  renderRow(signature) {
    return (
      <TouchableOpacity
        style={styles.tableRow}
        key={signature.contract_id.toString()}
        onPress={() => this.selectSignature(signature.contract_id)}
      >
        <View style={{ flex: 3, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {signature.payplan_name}
(
            {signature.contract_id}
)

          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {signature.employee_first_name}
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {signature.position_name}
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {signature.department_name}
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {signature.payplan_create_date}
          </Text>
        </View>
        <View style={{ flex: 2, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {signature.reviewed_by}
          </Text>
        </View>
        <View style={{ flex: 1, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {moment(new Date(signature.payplan_create_date))
              .fromNow()
              .replace(' days ago', 'd')
              .replace(' hours ago', 'h')
              .replace(' month ago', 'm')
              .replace(' years ago', 'y')}
          </Text>
        </View>
        <View style={{ flex: 1, alignSelf: 'center' }}>
          <Text style={{ color: '#000', fontSize: 13 }}>
            {moment().format('MM.DD.YYYY')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { employeeSignature, mySignature, changeTab, active } = this.props;
    const { activeTab } = this.state;
    return (
      <Container style={styles.container}>
        <StatusBar barStyle="dark-content" />
        <View style={styles.mine}>
          <View style={styles.actionsHeader}>
            <View style={styles.menu}>
              <Button
                style={styles.menuIconBtn}
                light
                onPress={() => this.props.navigation.navigate('DrawerOpen')}
              >
                <Icon name="ios-menu" style={styles.menuIcon} />
              </Button>
            </View>
            <View searchBar style={styles.searchBar}>
              <Item>
                <Icon name="ios-search" style={styles.iconSearch} />
                <Input placeholder="Search" placeholderTextColor="rgb(71,79,88)" />
              </Item>
            </View>
          </View>
          <View style={styles.tabsSection}>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                style={activeTab === 'employee' ? styles.tabActive : styles.tab}
                onPress={() => changeTab('employee')}
              >
                <Text style={activeTab === 'employee' ? styles.tabTextActive : styles.tabText}>
									Employee signature

                </Text>
                <View style={activeTab === 'employee' ? styles.badgeActive : styles.badge}>
                  <Text
                    style={activeTab === 'employee' ? styles.badgeTextActive : styles.badgeText}
                  >
                    {employeeSignature ? employeeSignature.length : 0}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={activeTab === 'my' ? styles.tabActive : styles.tab}
                onPress={() => changeTab('my')}
              >
                <Text style={activeTab === 'my' ? styles.tabTextActive : styles.tabText}>
									My signature

                </Text>
                <View style={activeTab === 'my' ? styles.badgeActive : styles.badge}>
                  <Text style={activeTab === 'my' ? styles.badgeTextActive : styles.badgeText}>
                    {mySignature ? mySignature.length : 0}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={[styles.latestTab, { borderBottomWidth: activeTab === 'latest' ? 4 : 0 }]}
              onPress={() => changeTab('latest')}
            >
              <Icon
                name="ios-clock-outline"
                style={[
                  styles.latestIcon,
                  {
                    color: activeTab === 'latest' ? 'rgb(0,119,255)' : 'rgb(71,79,88)',
                  },
                ]}
              />
              <Text
                style={{
                  color: activeTab === 'latest' ? 'rgb(0,119,255)' : 'rgb(71,79,88)',
                  fontSize: 14,
                }}
              >
								Latest Added

              </Text>
            </TouchableOpacity>
          </View>
          <Card style={styles.card}>
            <ScrollView>
              {activeTab === 'latest' ? (
                <View style={styles.tableContainer}>
                  
                  <View>
                    <Text style={styles.tableTitle}>
For Approval
                    </Text>
                    {this.renderHeader()}
                    <View>
                      {mySignature
												&& mySignature.map(signature => {
                          if(signature.contract_status_id === 4){
                            return this.renderRow(signature);
                          }
                        })}
                    </View>
                  </View>
                  <View>
                    <Text style={styles.tableTitle}>
Need Signature Contract
                    </Text>
                    {this.renderHeader()}
                    <View>
                      {mySignature
												&& mySignature.map(signature => this.renderRow(signature))}
                    </View>
                  </View>
                  <View>
                    <Text style={styles.tableTitle}>
Completed Contracts
                    </Text>
                    {this.renderHeader()}
                    <View>
                      {active
												&& active.map(signature => this.renderRow(signature))}
                    </View>
                  </View>
                </View>
              ) : (
                <View style={styles.tableContainer}>
                  {this.renderHeader()}
                  <View>
                    {activeTab === 'employee'
                      ? employeeSignature
											  && employeeSignature.map(signature => this.renderRow(signature))
                      : mySignature
											  && mySignature.map(signature => this.renderRow(signature))}
                  </View>
                </View>
              )}
            </ScrollView>
          </Card>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  employeeSignature: state.signatureEmployee.employeeSignature,
  mySignature: state.signatureMy.mySignature,
  tab: state.dashboardTab.tabName,
  active: state.active.active,
});

const mapDispatchToProps = dispatch => ({
  getEmployeeSignatureRequest: bindActionCreators(getEmployeeSignatureRequest, dispatch),
  getMySignatureRequest: bindActionCreators(getMySignatureRequest, dispatch),
  changeTab: bindActionCreators(changeTab, dispatch),
  getActiveRequest: bindActionCreators(getActiveRequest, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Dashboard);
