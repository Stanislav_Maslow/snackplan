import React, {Component} from 'react';
import {View, TextInput, Image, StatusBar} from 'react-native';
import {Container, Button, H3, Text, Icon, Form, Toast, Card} from 'native-base';
import IconFA from "react-native-vector-icons/FontAwesome";

import styles from './styles';

class ForgotPassword extends Component {
    state = {
        loading: false
    };

    resetPassword() {
        Toast.show({
            text: "This functional is under developing",
            buttonText: "Close",
            duration: 60000
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar barStyle="dark-content"/>
                <Form style={styles.column}>
                    <View style={{flex: 0.5}}/>
                    <Card style={styles.card}>
                        <View style={styles.row}>
                            <Image style={styles.image} source={ require('../../../assets/logo.png') }
                                   resizeMode="contain"/>
                        </View>
                        <View style={styles.row}>
                            <H3 style={styles.title}>Forgot password</H3>
                        </View>
                        <View style={styles.section}>
                            <IconFA active name="envelope-open-o" style={styles.icon} size={20}/>
                            <TextInput style={styles.input} placeholder="E-mail"
                                       placeholderTextColor="#CAE3FF"
                                       underlineColorAndroid="#0187ff"
                                       selectionColor="#0187ff"
                                       onChangeText={(email) => this.setState({email})}
                                       keyboardType="email-address" editable={!this.state.loading}/>
                        </View>
                        <View style={styles.row}>
                            <Button
                                title="Reset password"
                                style={styles.button}
                                onPress={this.resetPassword.bind(this)}
                                disabled={this.state.loading}
                            >
                                <Text style={{flex: 1, textAlign: 'left'}} uppercase={false}>Send</Text>
                                <Icon style={{flex: 1, textAlign: 'right'}} type="FontAwesome" name="angle-right"/>
                            </Button>
                        </View>
                    </Card>
                    <View style={{flex: 0.5}}/>

                </Form>
            </Container>
        );
    }

}

export default ForgotPassword;