import React, { Component } from 'react';
import ReactNative, {
	Image,
	TouchableOpacity,
	AsyncStorage,
	ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
	Content,
	Text,
	List,
	ListItem,
	View,
	Container,
	Spinner,
	Button,
	Icon,
	Card,
	CardItem
} from 'native-base';
var RCTUIManager = require('NativeModules').UIManager;
import styles from './style';
import { getContractDetailsRequest } from './../../redux/contractDetails';

class Signature extends Component {
	constructor(props) {
		super(props);
		this.state = {
			anchor: [],
			isLoading: false
		};
	}

	componentDidMount() {
		this.props.getContractDetailsRequest();
		this.setState(
			{
				isLoading: true
			},
			function() {}
		);
	}

	back() {
		this.props.navigation.navigate('Dashboard');
	}

	scrollIntoView(id) {
		this._contentScroll.scrollTo(0, 0, false);
		RCTUIManager.measure(
			ReactNative.findNodeHandle(this.state.anchor[id]),
			(fx, fy, width, height, px, py) => {
				this._contentScroll.scrollTo(py - 120, 0, true);
			}
		);
	}
	render() {
		const { contractDetails } = this.props;
		if (!contractDetails.CardGroups) {
			return (
				<Container
					style={{
						flex: 1,
						justifyContent: 'center',
						alignItems: 'center'
					}}
				>
					<Spinner color="#0077FF" />
				</Container>
			);
		}
		return (
			<Container style={styles.container}>
				<View
					style={{
						flexDirection: 'row',
						alignItems: 'center',
						height: 48,
						marginBottom: 31
					}}
				>
					<Button iconLeft style={styles.btnBack} onPress={() => this.back()}>
						<Icon
							name="ios-arrow-round-back"
							style={{ color: '#fff', fontSize: 30 }}
						/>
						<Text style={{ color: '#fff', marginRight: 10 }}>Back</Text>
					</Button>
					<Text style={styles.contractNumber}>
						Contract #4:10_06.28.2017_23492_Audi Mell
					</Text>
				</View>
				<View
					style={{
						flex: 1,
						flexDirection: 'row',
						justifyContent: 'flex-start'
					}}
				>
					<ScrollView style={styles.cardLink}>
						<Card>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(0)}>
									<Text>Dealership</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(1)}>
									<Text>Position</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(2)}>
									<Text>Employee Info</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(3)}>
									<Text>Schedule</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(4)}>
									<Text>Overtime</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(5)}>
									<Text>Draw</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(6)}>
									<Text>Salary</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(7)}>
									<Text>Base Comission</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(8)}>
									<Text>Enterprise/Taxi/Uber Bonus/Penalties</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(9)}>
									<Text>Length of LOAN (LOL) Bonus/Penalities</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(10)}>
									<Text>Saturday/Sunday Production</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(11)}>
									<Text>Total Store Production</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(12)}>
									<Text>Bonus</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(13)}>
									<Text>Penalty</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(14)}>
									<Text>Annual Discretionary Bonus</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(15)}>
									<Text>Attendance documentation</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(16)}>
									<Text>Training Charges</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(17)}>
									<Text>Training Charges</Text>
								</TouchableOpacity>
							</CardItem>
							<CardItem>
								<TouchableOpacity onPress={() => this.scrollIntoView(18)}>
									<Text>Circle of Excellence or Bonuses Disqualification</Text>
								</TouchableOpacity>
							</CardItem>
						</Card>
					</ScrollView>
					<ScrollView ref={c => (this._contentScroll = c)}>
						<Content
							bounces={false}
							style={styles.content}
							id="1"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card>
									<CardItem header bordered>
										<Text>Search results</Text>
									</CardItem>

									<View>
										<CardItem>
											<Text>
												CAPTION_CODE:{' '}
												{JSON.stringify(this.props.contractDetails, null, 4)}
											</Text>
										</CardItem>
									</View>
								</Card>

								{contractDetails.contract.cards.map(e => {
									if (e.card_name === 'Dealership Info') {
										return (
											<Card style={styles.card}>
												<View style={styles.header}>
													<Text style={styles.number}>01</Text>
													<Text style={styles.title}>{e.card_name}</Text>
												</View>
												<View style={styles.row}>
													<View style={styles.column}>
														<Text style={styles.label}>Name</Text>
														<Text style={styles.field}>
															{e.card_elements[1].value}
														</Text>
													</View>
													<View style={styles.column}>
														<Text style={styles.label}>Phone Number</Text>
														<Text style={styles.field}>
															{e.card_elements[2].value}
														</Text>
													</View>
												</View>
												<View style={styles.row}>
													<View style={styles.column}>
														<Text style={styles.label}>Adress</Text>
														<Text style={styles.field}>
															{e.card_elements[4].value}
														</Text>
													</View>
													<View style={styles.column}>
														<Text style={styles.label}>Website</Text>
														<Text style={styles.field}>
															{e.card_elements[3].value}
														</Text>
													</View>
												</View>
												<View style={styles.btnRow}>
													<Button iconRight style={styles.btn}>
														<Text style={{ marginLeft: 40 }}>I accept</Text>
														<Icon
															name="ios-checkmark"
															style={{
																color: '#fff',
																fontSize: 24,
																marginRight: 34
															}}
														/>
													</Button>
												</View>
											</Card>
										);
									}
								})}
							</View>
						</Content>

						<Content
							bounces={false}
							style={styles.content}
							id="2"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>02</Text>
										<Text style={styles.title}>Position</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Name</Text>
											<Text style={styles.field}>Sales Manager</Text>
											<Text style={styles.label}>
												Short description text about position Dvd Replication
												For Dummies 4 Easy Steps To Professional Dvd Replication
											</Text>
										</View>
										<View style={styles.column}>
											<Text style={styles.label}>Effective</Text>
											<Text style={styles.field}>11 March, 2017</Text>
										</View>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Supervisor</Text>
											<Text style={styles.field}>Emanuel Frederick</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>

						<Content
							bounces={false}
							style={styles.content}
							id="3"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>03</Text>
										<Text style={styles.title}>Employee Info</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Name</Text>
											<Text style={styles.field}>Audi Arligton</Text>
										</View>
										<View style={styles.column}>
											<Text style={styles.label}>Phone Number</Text>
											<Text style={styles.field}>253-107-5230</Text>
										</View>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Adress</Text>
											<Text style={styles.field}>
												Adress 4022, Liberti boulevad 22
											</Text>
										</View>
										<View style={styles.column}>
											<Text style={styles.label}>Effective</Text>
											<Text style={styles.field}>December 12, 2016</Text>
										</View>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Email</Text>
											<Text style={styles.field}>delta_wolff@pacocha.me</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="4"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>04</Text>
										<Text style={styles.title}>Schedule</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.field,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											Employee is expected to work enough hours to ensure the
											successful operation of the Audi Service Department.{' '}
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="5"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>05</Text>
										<Text style={styles.title}>Overtime</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.field,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											Additional hours may be necessary to cover for vacations,
											training or other circumstances.
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="6"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>06</Text>
										<Text style={styles.title}>Draw</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Monthly</Text>
											<Text style={styles.field}>$10,000</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="7"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>07</Text>
										<Text style={styles.title}>Salary</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Monthly</Text>
											<Text style={styles.field}>$60,000</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="8"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>08</Text>
										<Text style={styles.title}>Base Comission</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Comission</Text>
											<Text style={styles.field}>%5.00</Text>
											<Text style={styles.label}>
												Short description text about position Dvd Replication
												For Dummies 4 Easy Steps  To Professional Dvd
												Replication
											</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="9"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>09</Text>
										<Text style={styles.title}>
											Enterprise/Taxi/Uber Bonus/Penalties
										</Text>
									</View>
									<View
										style={[
											styles.row,
											{ marginBottom: 30, paddingLeft: 10, paddingRight: 10 }
										]}
									>
										<Text style={styles.field}>
											This will measure the amount of time a customer is using
											the company provided Loaner Cars. The TSD system provides
											a report that measure LOL. Shorter LOL is desired by
											company, since this means fewer Loaner Cars must be in
											Service to meet the needs of the customers.
										</Text>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>a.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>
												0.25% Additional Monthly Commission of Net Contribution
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10, marginBottom: 30 }}>
											<Text style={styles.contextBg}>
												if LOL is below 2.75 days for the month for the entire
												store, or
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>b.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>
												0.25% Deduction from Monthly Commission of Net
												Contribution ​​​​​​​
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10 }}>
											<Text style={styles.contextBg}>
												if LOL is above 3.25 days for the month for the entire
												store, or
											</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="10"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>10</Text>
										<Text style={styles.title}>
											Length of LOAN (LOL) Bonus/Penalities
										</Text>
									</View>
									<View
										style={[
											styles.row,
											{ marginBottom: 30, paddingLeft: 10, paddingRight: 10 }
										]}
									>
										<Text style={styles.field}>
											Service Department Contribution is defined as Service
											Gross Profit, less the following expenses:
										</Text>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>a.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>
												0.25% Additional Monthly Commission of Net Contribution
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10, marginBottom: 30 }}>
											<Text style={styles.contextBg}>
												Enterprise/Taxi/Uber Spend is below $1,500 for the
												month, or
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>b.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>
												0.25% Deduction of Monthly Commission of Net
												Contribution​​​​​​​
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10, marginBottom: 30 }}>
											<Text style={styles.contextBg}>
												Enterprise/Taxi/Uber Spend is above $2,500 for the
												month, or
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>c.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>0.50% Deduction</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10 }}>
											<Text style={styles.contextBg}>
												if Enterprise/Taxi/Uber spend is above $3,500 for the
												month.
											</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="11"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>11</Text>
										<Text style={styles.title}>Saturday/Sunday Production</Text>
									</View>
									<View
										style={[
											styles.row,
											{ marginBottom: 30, paddingLeft: 10, paddingRight: 10 }
										]}
									>
										<Text style={styles.field}>
											Service Department Contribution is defined as Service
											Gross Profit, less the following expenses:
										</Text>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>a.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>
												0.25% Additional Monthly Commission of Net Contribution
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10 }}>
											<Text style={styles.contextBg}>
												if Saturday and Sunday production of Service and Parts
												Gross Profit for Customer Pay, Warranty, Sublet, Gas/Oil
												and Tires, net of discounts exceeds $125,000 for the
												month.
											</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="12"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>12</Text>
										<Text style={styles.title}>Total Store Production</Text>
									</View>
									<View
										style={[
											styles.row,
											{ marginBottom: 30, paddingLeft: 10, paddingRight: 10 }
										]}
									>
										<Text style={styles.field}>
											Service Department Contribution is defined as Service
											Gross Profit, less the following expenses:
										</Text>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }}>
											<Text style={styles.tag}>a.</Text>
										</View>
										<View style={styles.rowBgContext}>
											<Text style={styles.contextBg}>
												0.25% Additional Monthly Commission of Net Contribution
											</Text>
										</View>
									</View>
									<View style={styles.rowBg}>
										<View style={{ flex: 1 }} />
										<View style={{ flex: 10 }}>
											<Text style={styles.contextBg}>
												if Audi Service and Parts Net Contribution exceeds
												$250,000 for the month
											</Text>
										</View>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="13"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>13</Text>
										<Text style={styles.title}>Bonus</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Bonus</Text>
											<Text style={styles.field}>$1000</Text>
										</View>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											if SSI is at, or above, National Average for the month and
											Email penetration must be above required threshold
											(currently 80%).
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="14"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>14</Text>
										<Text style={styles.title}>Penalty</Text>
									</View>
									<View style={styles.row}>
										<View style={styles.column}>
											<Text style={styles.label}>Penalty</Text>
											<Text style={styles.field}>$10</Text>
										</View>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											Penalty to the monthly commission will be in effect if the
											Audi Service Department score is below National Average
											for the month or if e-mail capture rate is below the
											required threshold established by Audi of America for
											payout (currently 80%).
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="15"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>15</Text>
										<Text style={styles.title}>Annual Discretionary Bonus</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											In December of each Calendar year, the company will review
											the performance of the Audi Service operation and will
											determine if a discretionary bonus is appropriate. The
											Annual Discretionary Bonus will be paid in January of the
											following year. I understand that I must be employed with
											the company in order to be paid the
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="16"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>16</Text>
										<Text style={styles.title}>Attendance documentation</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											You are required by federal and state law and by the
											Company to keep an accurate record of all hours your work
											each day by clocking in and out of the electronic time
											clock system.
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="17"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>17</Text>
										<Text style={styles.title}>Training Charges</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											The Company provides different opportunities for Employees
											to further their development through training. This
											includes training provided by Geneva Management, the
											Manufacturer, Finance Companies and other third parties.
											Generally, the Company pays for this training and the
											Employees are not charged. ​​​​​​​The Company provides
											different opportunities for Employees to further their
											development through training
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="18"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>18</Text>
										<Text style={styles.title}>Training Charges</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											If an Employee is in possession of a Company owned
											vehicle, lease turn in, trade in, a Customer’s vehicle or
											any other vehicle entrusted to the Company and the vehicle
											is damaged, the Employee MUST immediately notify their
											supervisor of the damage. The supervisor must fill out an
											Accident Report with all of the necessary information
											about the circumstances leading to the damage. When the
											damage to a vehicle is known, the situation can be
											addressed, repairs made
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
						<Content
							bounces={false}
							style={styles.content}
							id="19"
							ref={c => this.state.anchor.push(c)}
						>
							<View style={styles.contentView}>
								<Card style={styles.card}>
									<View style={styles.header}>
										<Text style={styles.number}>19</Text>
										<Text style={styles.title}>
											Circle of Excellence or Bonuses Disqualification
										</Text>
									</View>
									<View style={styles.row}>
										<Text
											style={[
												styles.label,
												{ paddingLeft: 10, paddingRight: 10 }
											]}
										>
											Employee acknowledges, understands and agrees that if, as
											a result of Employee’s negligence, actions or omissions,
											the Company suffers material financial loss from a Repair
											Order, Loaner Car Transaction, or Parts Ticket then, under
											such circumstances as a legitimate disciplinary measure,
											Employee understands and agrees that Employee may be
											disqualified from receiving any Circle of Excellence
											award, bonus monies, etc. and/or be disqualified from
											receiving any discretionary bonuses. The determination to
											disqualify Employee from any referenced bonus/award will
											be made by the Company in its sole and absolute judgment
											and discretion based upon the facts and circumstances
											involved. I understand that my employment at Audi
											Arlington is AT WILL, and may be terminated or I may
											voluntarily leave the employment of the company at any
											time. This policy has been in effect since the start of my
											employment. No pay plan, commission schedule, or any other
											form or arrangement constitutes a contract for employment.
										</Text>
									</View>
									<View style={styles.btnRow}>
										<Button iconRight style={styles.btn}>
											<Text style={{ marginLeft: 40 }}>I accept</Text>
											<Icon
												name="ios-checkmark"
												style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
											/>
										</Button>
									</View>
								</Card>
							</View>
						</Content>
					</ScrollView>
				</View>
			</Container>
		);
	}
}

const mapStateToProps = state => {
	return {
		contractDetails: state.contractDetails.contractDetails
	};
};

const mapDispatchToProps = dispatch => ({
	getContractDetailsRequest: bindActionCreators(
		getContractDetailsRequest,
		dispatch
	)
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Signature);
