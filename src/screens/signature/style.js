const React = require('react-native');

const { Platform, Dimensions, PixelRatio } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const contentView = (deviceWidth / 4) * 2.7;
const menuWidth = (deviceWidth / 4) * 1;
const scale = deviceWidth / 320;

function normilize(size) {
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(size));
  }
  return Math.round(PixelRatio.roundToNearestPixel(size)) - 2;
}

export default {
  content: {
    flex: 1,
    top: 0,
    marginBottom: 30,
  },
  container: {
    backgroundColor: '#FAFBFC',
    paddingTop: 50,
  },
  contentView: {
    width: contentView,
    // marginLeft: 200
  },
  card: {
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight: 10,
  },
  header: {
    height: 118,
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#ECF1F6',
    paddingLeft: 10,
  },
  number: {
    fontSize: normilize(24),
    color: 'rgb(0,119,255)',
  },
  title: {
    fontSize: normilize(28),
    color: 'rgb(71,79,88)',
    paddingLeft: 15,
  },
  label: {
    color: '#CFD2D5',
    fontSize: normilize(13),
  },
  field: {
    color: '#3F4953',
    fontSize: normilize(18),
  },
  column: {
    flexDirection: 'column',
    width: contentView / 2 - 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  btnRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  btn: {
    backgroundColor: 'rgb(97,189,12)',
    width: 175,
    height: 60,
    marginLeft: 10,
  },
  btnBlue: {
    backgroundColor: '#3392FF',
    width: 175,
    height: 60,
    marginLeft: 10,
  },
  btnTransparent: {
    backgroundColor: 'transparent',
    position: 'absolute',
    width: 140,
    height: 40,
    top: -5,
    right: 0,
    marginBottom: 20,
    shadowColor: 'white',
    shadowOffset: { width: 0, height: 0 },
  },
  btnBack: {
    width: 110,
    height: 48,
    marginLeft: 20,
    backgroundColor: '#B6B9BE',
  },
  contractNumber: {
    fontSize: normilize(28),
    color: '#8E949C',
    paddingLeft: 30,
  },
  rowBg: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 10,
    paddingLeft: 10,
  },
  rowBgContext: {
    flex: 10,
    backgroundColor: '#E9EAEC',
    padding: 10,
    marginBottom: 10,
  },
  tag: {
    fontSize: normilize(28),
    color: '#8E949C',
  },
  contextBg: {
    fontSize: normilize(14),
    color: 'rgb(71,79,88)',
  },
  cardLink: {
    width: menuWidth,
    marginRight: 19,
  },
  signButton: {
    marginTop: 5,
    marginRight: 12,
    marginBottom: 5,
    flex: 0.7,
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: '#0077FF',
    borderRadius: 5,
    height: 50,
    paddingLeft: 10,
    paddingRight: 10,
  },
  sketch: {
    flex: 1,
  },
  sketchContainer: {
    height: '20%',
    width: 200,
  },
  image: {
    flex: 1,
  },
  imageContainer: {
    height: '20%',
    width: 200,
    borderTopWidth: 4,
    borderTopColor: '#E44262',
  },
  labelSign: {
    width: '100%',
    padding: 5,
    alignItems: 'center',
  },
  button: {
    // position: 'absolute',
    // bottom: 8,
    // left: 8,
    zIndex: 1,
    padding: 12,
    minWidth: 56,
    minHeight: 48,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    height: '40%',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    height: '40%',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalHeader: {
    flexDirection: 'row',
    marginBottom: 15,
    position: 'relative',
  },
};
