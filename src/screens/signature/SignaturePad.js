import * as ExpoPixi from 'expo-pixi';
import React, { Component } from 'react';
import { Platform, AppState, StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

import { Button, Text } from 'native-base';

const isAndroid = Platform.OS === 'android';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 250,
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnContainer: {
    flex: 1,
    height: 30,
    width: '80%',
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
  },
  sketchContainer: {
    height: '60%',
    width: '80%',
    borderWidth: 0.5,
    borderColor: 'grey',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sketch: {
    flex: 1,
  },
  image: {
    flex: 1,
  },
  label: {
    width: '100%',
    padding: 5,
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  button: {
    zIndex: 1,
    padding: 12,
    minWidth: 56,
    minHeight: 48,
  },
});

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0;
    const v = c === 'x' ? r : (r & 0x3) | 0x8;

    return v.toString(16);
  });
}

export default class SignPad extends Component {
  static propTypes = {
    submitSignature: PropTypes.func,
    styles: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    submitSignature: () => {},
    styles: {},
  };

  state = {
    dataUrl: null,
    appState: AppState.currentState,
  };

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChangeAsync);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChangeAsync);
  }

  onChangeAsync = async () => {
    // const options = {
    //   format: 'png', /// PNG because the view has a clear background
    //   quality: 0.1, /// Low quality works because it's just a line
    //   result: 'file',
    //   height,
    //   width
    // };
    // const uri = await Expo.takeSnapshotAsync(this.sketch, options);
    const { uri } = await this.sketch.takeSnapshotAsync();

    this.toDataURL(uri).then(dataUrl => {
      this.setState({
        dataUrl,
      });
    });
  };

  onReady = () => {
    console.log('ready!');
  };

  handleAppStateChangeAsync = nextAppState => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      if (isAndroid && this.sketch) {
        this.setState({ appState: nextAppState, id: uuidv4(), lines: this.sketch.lines });
        return;
      }
    }
    this.setState({ appState: nextAppState });
  };

  toDataURL = url =>
    fetch(url)
      .then(response => response.blob())
      .then(
        blob =>
          new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.onerror = reject;
            reader.readAsDataURL(blob);
          })
      );

  submitSignature = () => {
    const { submitSignature } = this.props;
    submitSignature(this.state.dataUrl);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.sketchContainer}>
          <ExpoPixi.Signature
            ref={ref => (this.sketch = ref)}
            style={styles.sketch}
            strokeColor="blue"
            strokeAlpha={1}
            strokeWidth={2}
            onReady={this.onReady}
            onChange={this.onChangeAsync}
          />
        </View>
        <View style={styles.btnContainer}>
          <Button
            style={styles.button}
            primary
            onPress={() => {
              this.sketch.undo();
            }}
          >
            <Text>Clear</Text>
          </Button>
          <Button success style={styles.button} onPress={this.submitSignature}>
            <Text> Done </Text>
          </Button>
        </View>
      </View>
    );
  }
}
