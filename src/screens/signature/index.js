import React, { Component } from 'react';
import ReactNative, { TouchableOpacity, ScrollView, Image } from 'react-native';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Content, Text, View, Container, Spinner, Button, Icon, Card, CardItem } from 'native-base';
import SignPad from './SignaturePad';
import styles from './style';
import { getContractDetailsRequest, putSignatureRequest } from '../../redux/contractDetails';

const RCTUIManager = require('NativeModules').UIManager;

class Signature extends Component {
  static propTypes = {
    getContractDetailsRequest: PropTypes.func,
    putSignatureRequest: PropTypes.func,
    putStatus: PropTypes.number,
    contractDetails: PropTypes.objectOf(PropTypes.any),
    navigation: PropTypes.objectOf(PropTypes.any),
  };

  static defaultProps = {
    getContractDetailsRequest: () => {},
    putSignatureRequest: () => {},
    putStatus: 0,
    contractDetails: {},
    navigation: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      anchor: [],
      cardNumber: 0,
      dealershipClicked: false,
      departmentClicked: false,
      employeeInfoClicked: false,
      overtimeClicked: false,
      drawClicked: false,
      salaryClicked: false,
      scheduleClicked: false,
      comissionClicked: false,
      basicTrainingClicked: false,
      taxiClicked: false,
      LOANClicked: false,
      penaltyClicked: false,
      discretionaryClicked: false,
      attendanceClicked: false,
      trainingChargesClicked: false,
      trainingClicked: false,
      circleClicked: false,
      clickedCounter: 0,
      showSignPad: false,
      isModalVisible: false,
    };
  }

  componentDidMount() {
    const itemId = this.props.navigation.getParam('itemId', 'NO-ID');
    this.props.getContractDetailsRequest(itemId);
  }

  
  scrollIntoView(id) {
    this.contentScroll.scrollTo(0, 0, false);
    RCTUIManager.measure(
      ReactNative.findNodeHandle(this.state.anchor[id]),
      (fx, fy, width, height, px, py) => {
        this.contentScroll.scrollTo(py - 120, 0, true);
      }
    );
  }

  acceptButtonClick = cardName => {
    if (!this.state[cardName]) {
      this.setState({
        [cardName]: true,
        clickedCounter: this.state.clickedCounter + 1,
      });
    }
  };
  
  submitSignature = sign => {
    const contractData = {
      action_value: '',
      contract_id: this.props.contractDetails.params.contract_id,
      dealership_id: this.props.contractDetails.params.DEALERSHIP_ID,
    };
    if (this.props.contractDetails.contract.contract_status_id === 3) {
      contractData.action_value = 4;
      contractData.employee_signature = sign;
    } else {
      contractData.action_value = 5;
      contractData.approver_signature = sign;
    }
    this.setState({
      showSignPad: false,
      isModalVisible: true,
    });
    this.props.putSignatureRequest(contractData);
  };
  showSignPad = () => this.setState({ showSignPad: !this.state.showSignPad });

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
    this.props.navigation.navigate('Dashboard');
  };

  back() {
    this.props.navigation.navigate('Dashboard');
  }
  
  render() {
    const { contractDetails } = this.props;
    const {
      dealershipClicked,
      departmentClicked,
      employeeInfoClicked,
      overtimeClicked,
      drawClicked,
      salaryClicked,
      scheduleClicked,
      comissionClicked,
      basicTrainingClicked,
      taxiClicked,
      LOANClicked,
      penaltyClicked,
      discretionaryClicked,
      attendanceClicked,
      trainingChargesClicked,
      trainingClicked,
      circleClicked,
    } = this.state;

    if (!contractDetails.CardGroups) {
      return (
        <Container
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Spinner color="#0077FF" />
        </Container>
      );
    }
    return (
      <Container style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            height: 48,
            marginBottom: 31,
          }}
        >
          <Button iconLeft style={styles.btnBack} onPress={() => this.back()}>
            <Icon name="ios-arrow-round-back" style={{ color: '#fff', fontSize: 30 }} />
            <Text style={{ color: '#fff', marginRight: 10 }}>Back</Text>
          </Button>
          <Text style={styles.contractNumber}>Contract #4:10_06.28.2017_23492_Audi Mell </Text>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
          <ScrollView style={styles.cardLink}>
            <Card>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(0)}>
                  <Text>Dealership </Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(1)}>
                  <Text>Position</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(2)}>
                  <Text>Employee Info </Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(3)}>
                  <Text>Schedule</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(4)}>
                  <Text>Overtime</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(5)}>
                  <Text>Draw</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(6)}>
                  <Text>Salary</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(7)}>
                  <Text>Base Comission</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(8)}>
                  <Text>Enterprise/Taxi/Uber Bonus/Penalties</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(9)}>
                  <Text>Length of LOAN (LOL) Bonus/Penalities</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(10)}>
                  <Text>Saturday/Sunday Production</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(11)}>
                  <Text>Total Store Production</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(12)}>
                  <Text>Bonus</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(13)}>
                  <Text>Penalty</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(14)}>
                  <Text>Annual Discretionary Bonus</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(15)}>
                  <Text>Attendance documentation</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(16)}>
                  <Text>Training Charges</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(17)}>
                  <Text>Training Charges</Text>
                </TouchableOpacity>
              </CardItem>
              <CardItem>
                <TouchableOpacity onPress={() => this.scrollIntoView(18)}>
                  <Text>Circle of Excellence or Bonuses Disqualification </Text>
                </TouchableOpacity>
              </CardItem>
            </Card>
          </ScrollView>
          <ScrollView ref={c => (this.contentScroll = c)}>
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Dealership Info' && e.card_elements.length) {
                return (
                  <Content
                    key={e.card_name}
                    bounces={false}
                    style={styles.content}
                    id="1"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>01</Text>
                          <Text style={styles.title}>{e.card_name}</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Name</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Name') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                          <View style={styles.column}>
                            <Text style={styles.label}>Phone Number</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Phone Number') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Address</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Address') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'City') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'State') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'Zip') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                          <View style={styles.column}>
                            <Text style={styles.label}>Website</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Website') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={dealershipClicked ? styles.btn : styles.btnBlue}
                                onPress={() => {
                                  this.acceptButtonClick('dealershipClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{
                                    color: '#fff',
                                    fontSize: 24,
                                    marginRight: 34,
                                  }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Department/Position' && e.card_elements.length) {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="2"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>02</Text>
                          <Text style={styles.title}>Position</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Name</Text>
                            <Text style={styles.field}>{e.card_elements[3].value}</Text>
                            <Text style={styles.label}>
                              Short description that is not provided in JSON yet
                            </Text>
                          </View>
                          <View style={styles.column}>
                            <Text style={styles.label}>Effective</Text>
                            <Text style={styles.field}>
                              {contractDetails.contract.cards.map(el => {
                                if (el.card_name === 'Effective Date') {
                                  return el.card_elements[0].value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Supervisor</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Supervisor Name') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={departmentClicked ? styles.btn : styles.btnBlue}
                                onPress={() => {
                                  this.acceptButtonClick('departmentClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{
                                    color: '#fff',
                                    fontSize: 24,
                                    marginRight: 34,
                                  }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Employee Information' && e.card_elements.length) {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="3"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>03</Text>
                          <Text style={styles.title}>Employee Info</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Name</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'First Name') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'Middle Name') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'Last Name') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                          <View style={styles.column}>
                            <Text style={styles.label}>Phone Number</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Phone Number') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Address</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Address') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'City') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'State') {
                                  return item.value;
                                }
                              })}{' '}
                              {e.card_elements.map(item => {
                                if (item.label === 'Zip') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                          <View style={styles.column}>
                            <Text style={styles.label}>Effective</Text>
                            <Text style={styles.field}>December 12, 2016</Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Email</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Email') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={employeeInfoClicked ? styles.btn : styles.btnBlue}
                                onPress={() => {
                                  this.acceptButtonClick('employeeInfoClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Schedule') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="4"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>04</Text>
                          <Text style={styles.title}>Schedule</Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.field, { paddingLeft: 10, paddingRight: 10 }]}>
                            {e.description}
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={scheduleClicked ? styles.btn : styles.btnBlue}
                                onPress={() => {
                                  this.acceptButtonClick('scheduleClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Overtime') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="5"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>05</Text>
                          <Text style={styles.title}>Overtime</Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.field, { paddingLeft: 10, paddingRight: 10 }]}>
                            Additional hours may be necessary to cover for vacations, training or
                            other circumstances.
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={overtimeClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('overtimeClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Draw') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="6"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>06</Text>
                          <Text style={styles.title}>Draw</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Monthly</Text>
                            <Text style={styles.field}>$10,000</Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={drawClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('drawClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Salary') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="7"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>07</Text>
                          <Text style={styles.title}>Salary</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Monthly</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Verbos') {
                                  return item.value;
                                }
                              })}{' '}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={salaryClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('salaryClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Base Comission') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    id="8"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>08</Text>
                          <Text style={styles.title}>Base Comission</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Comission</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item =>
                                 item.label === 'Comission' && item.value
                                )}
                            </Text>
                            <Text style={styles.label}>
                              Short description text about position Dvd Replication For Dummies 4
                              Easy Steps To Professional Dvd Replication
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={comissionClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('comissionClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Basic Training') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    key={e.card_name}
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>08</Text>
                          <Text style={styles.title}>Basic Training</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Basic Training</Text>
                            <Text style={styles.field}>
                              {e.card_elements.map(item => {
                                if (item.label === 'Basic Training') {
                                  return item.value;
                                }
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.field, { paddingLeft: 10, paddingRight: 10 }]}>
                            {e.card_elements.map(item => {
                              if (item.label === 'Basic Training') {
                                return item.description;
                              }
                            })}
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={basicTrainingClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('basicTrainingClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name.indexOf('Taxi') > 0) {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="9"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>09</Text>
                          <Text style={styles.title}>Enterprise/Taxi/Uber Bonus/Penalties</Text>
                        </View>
                        <View
                          style={[
                            styles.row,
                            { marginBottom: 30, paddingLeft: 10, paddingRight: 10 },
                          ]}
                        >
                          <Text style={styles.field}>
                            This will measure the amount of time a customer is using the company
                            provided Loaner Cars. The TSD system provides a report that measure LOL.
                            Shorter LOL is desired by company, since this means fewer Loaner Cars
                            must be in Service to meet the needs of the customers.
                          </Text>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>a.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>
                              {' '}
                              0.25% Additional Monthly Commission of Net Contribution{' '}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10, marginBottom: 30 }}>
                            <Text style={styles.contextBg}>
                              if LOL is below 2.75 days for the month for the entire store, or
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>b.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>
                              0.25% Deduction from Monthly Commission of Net Contribution
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10 }}>
                            <Text style={styles.contextBg}>
                              if LOL is above 3.25 days for the month for the entire store, or
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={taxiClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('taxiClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept </Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(
              e =>
                e.card_name.indexOf('LOAN') && (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="10"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>10</Text>
                          <Text style={styles.title}>Length of LOAN (LOL) Bonus/Penalities</Text>
                        </View>
                        <View
                          style={[
                            styles.row,
                            { marginBottom: 30, paddingLeft: 10, paddingRight: 10 },
                          ]}
                        >
                          <Text style={styles.field}>
                            Service Department Contribution is defined as Service Gross Profit, less
                            the following expenses:{' '}
                          </Text>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>a.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>
                              0.25% Additional Monthly Commission of Net Contribution
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10, marginBottom: 30 }}>
                            <Text style={styles.contextBg}>
                              Enterprise/Taxi/Uber Spend is below $1,500 for the month, or
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>b.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>
                              0.25% Deduction of Monthly Commission of Net Contribution
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10, marginBottom: 30 }}>
                            <Text style={styles.contextBg}>
                              Enterprise/Taxi/Uber Spend is above $2,500 for the month, or
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>c.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>0.50% Deduction</Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10 }}>
                            <Text style={styles.contextBg}>
                              if Enterprise/Taxi/Uber spend is above $3,500 for the month.
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={LOANClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('LOANClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                )
            )}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name.indexOf('Sunday') > 0) {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="11"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>11</Text>
                          <Text style={styles.title}>Saturday/Sunday Production</Text>
                        </View>
                        <View
                          style={[
                            styles.row,
                            { marginBottom: 30, paddingLeft: 10, paddingRight: 10 },
                          ]}
                        >
                          <Text style={styles.field}>
                            Service Department Contribution is defined as Service Gross Profit, less
                            the following expenses:
                          </Text>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>a.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>
                              0.25% Additional Monthly Commission of Net Contribution
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10 }}>
                            <Text style={styles.contextBg}>
                              if Saturday and Sunday production of Service and Parts Gross Profit
                              for Customer Pay, Warranty, Sublet, Gas/Oil and Tires, net of
                              discounts exceeds $125,000 for the month.
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={LOANClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('LOANClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Total Store Production') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="12"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>12</Text>
                          <Text style={styles.title}>Total Store Production</Text>
                        </View>
                        <View
                          style={[
                            styles.row,
                            { marginBottom: 30, paddingLeft: 10, paddingRight: 10 },
                          ]}
                        >
                          <Text style={styles.field}>
                            Service Department Contribution is defined as Service Gross Profit, less
                            the following expenses:
                          </Text>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.tag}>a.</Text>
                          </View>
                          <View style={styles.rowBgContext}>
                            <Text style={styles.contextBg}>
                              0.25% Additional Monthly Commission of Net Contribution
                            </Text>
                          </View>
                        </View>
                        <View style={styles.rowBg}>
                          <View style={{ flex: 1 }} />
                          <View style={{ flex: 10 }}>
                            <Text style={styles.contextBg}>
                              if Audi Service and Parts Net Contribution exceeds $250,000 for the
                              month
                            </Text>
                          </View>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={LOANClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('LOANClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Bonus') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="13"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>13</Text>
                          <Text style={styles.title}>Bonus</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Bonus</Text>
                            <Text style={styles.field}>$1000</Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            if SSI is at, or above, National Average for the month and Email
                            penetration must be above required threshold (currently 80%).
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={LOANClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('LOANClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Penalty') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="14"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>14</Text>
                          <Text style={styles.title}>Penalty</Text>
                        </View>
                        <View style={styles.row}>
                          <View style={styles.column}>
                            <Text style={styles.label}>Penalty</Text>
                            <Text style={styles.field}>$10</Text>
                          </View>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            Penalty to the monthly commission will be in effect if the Audi Service
                            Department score is below National Average for the month or if e-mail
                            capture rate is below the required threshold established by Audi of
                            America for payout (currently 80%).
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={penaltyClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('penaltyClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Annual Discretionary Bonus') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="15"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>15</Text>
                          <Text style={styles.title}>Annual Discretionary Bonus</Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            In December of each Calendar year, the company will review the
                            performance of the Audi Service operation and will determine if a
                            discretionary bonus is appropriate. The Annual Discretionary Bonus will
                            be paid in January of the following year. I understand that I must be
                            employed with the company in order to be paid the
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={discretionaryClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('discretionaryClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Attendance documentation') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="16"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>16</Text>
                          <Text style={styles.title}>Attendance documentation</Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            You are required by federal and state law and by the Company to keep an
                            accurate record of all hours your work each day by clocking in and out
                            of the electronic time clock system.
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={attendanceClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('attendanceClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Training Charges') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="17"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>17</Text>
                          <Text style={styles.title}>Training Charges</Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            The Company provides different opportunities for Employees to further
                            their development through training. This includes training provided by
                            Geneva Management, the Manufacturer, Finance Companies and other third
                            parties. Generally, the Company pays for this training and the Employees
                            are not charged. The Company provides different opportunities for
                            Employees to further their development through training
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={trainingChargesClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('trainingChargesClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Training Charges') {
                return (
                  <Content
                    bounces={false}
                    style={styles.content}
                    id="18"
                    ref={c => this.state.anchor.push(c)}
                    key={e.card_name}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>18</Text>
                          <Text style={styles.title}>Training Charges</Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            If an Employee is in possession of a Company owned vehicle, lease turn
                            in, trade in, a Customer’s vehicle or any other vehicle entrusted to the
                            Company and the vehicle is damaged, the Employee MUST immediately notify
                            their supervisor of the damage. The supervisor must fill out an Accident
                            Report with all of the necessary information about the circumstances
                            leading to the damage. When the damage to a vehicle is known, the
                            situation can be addressed, repairs made
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={trainingClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('trainingClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}
            {contractDetails.contract.cards.map(e => {
              if (e.card_name === 'Circle of Excellence or Bonuses Disqualification') {
                return (
                  <Content
                    key={e.card_name}
                    bounces={false}
                    style={styles.content}
                    id="19"
                    ref={c => this.state.anchor.push(c)}
                  >
                    <View style={styles.contentView}>
                      <Card style={styles.card}>
                        <View style={styles.header}>
                          <Text style={styles.number}>19</Text>
                          <Text style={styles.title}>
                            {' '}
                            Circle of Excellence or Bonuses Disqualification{' '}
                          </Text>
                        </View>
                        <View style={styles.row}>
                          <Text style={[styles.label, { paddingLeft: 10, paddingRight: 10 }]}>
                            Employee acknowledges, understands and agrees that if, as a result of
                            Employee’s negligence, actions or omissions, the Company suffers
                            material financial loss from a Repair Order, Loaner Car Transaction, or
                            Parts Ticket then, under such circumstances as a legitimate disciplinary
                            measure, Employee understands and agrees that Employee may be
                            disqualified from receiving any Circle of Excellence award, bonus
                            monies, etc. and/or be disqualified from receiving any discretionary
                            bonuses. The determination to disqualify Employee from any referenced
                            bonus/award will be made by the Company in its sole and absolute
                            judgment and discretion based upon the facts and circumstances involved.
                            I understand that my employment at Audi Arlington is AT WILL, and may be
                            terminated or I may voluntarily leave the employment of the company at
                            any time. This policy has been in effect since the start of my
                            employment. No pay plan, commission schedule, or any other form or
                            arrangement constitutes a contract for employment.
                          </Text>
                        </View>
                        <View style={styles.btnRow}>
                          {contractDetails.contract.contract_status_id === 3 && (
                            <View style={styles.btnRow}>
                              <Button
                                iconRight
                                style={circleClicked ? styles.btnBlue : styles.btn}
                                onPress={() => {
                                  this.acceptButtonClick('circleClicked');
                                }}
                              >
                                <Text style={{ marginLeft: 40 }}>I accept</Text>
                                <Icon
                                  name="ios-checkmark"
                                  style={{ color: '#fff', fontSize: 24, marginRight: 34 }}
                                />
                              </Button>
                            </View>
                          )}
                        </View>
                      </Card>
                    </View>
                  </Content>
                );
              }
            })}

            {contractDetails.contract.contract_status_id === 4 &&
              contractDetails.CURRENT_USER_IS_APPROVER === 1 && (
                <View>
                  <View
                    style={{
                      backgroundColor: 'white',
                      padding: 22,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 4,
                      borderColor: 'rgba(0, 0, 0, 0.5)',
                    }}
                  >
                    <Text style={{ flex: 1, textAlign: 'left' }} uppercase={false}>
                      Employee Signature
                    </Text>
                    <Image
                      style={{
                        width: 420,
                        height: 150,
                        marginTop: 10,
                        borderWidth: 0.5,
                        borderColor: 'grey',
                      }}
                      source={{
                        uri: `${contractDetails.contract.signature_url}`,
                      }}
                    />
                    <Text style={{ flex: 1, textAlign: 'left' }} uppercase={false}>
                      Date: {contractDetails.contract.signature_date}
                    </Text>

                    <Button
                      title="Ready to Sign"
                      style={styles.signButton}
                      onPress={this.showSignPad}
                      light
                    >
                      <Text
                        style={{ flex: 1, textAlign: 'left', color: 'white' }}
                        uppercase={false}
                      >
                        Ready to Sign
                      </Text>
                      <Icon
                        style={{ flex: 1, textAlign: 'right', color: 'white' }}
                        type="FontAwesome"
                        name="angle-right"
                      />
                    </Button>

                    <Modal
                      isVisible={this.state.showSignPad && this.props.putStatus === 0}
                      style={styles.bottomModal}
                    >
                      <View style={styles.modalContent}>
                        <View style={styles.modalHeader}>
                          <Text
                            style={{
                              flex: 1,
                              fontSize: 23,
                              color: 'rgb(71, 79, 88)',
                              textAlign: 'center',
                            }}
                            uppercase={false}
                          >
                            Signature
                          </Text>
                          <Button onPress={this.showSignPad} style={styles.btnTransparent}>
                            <Text
                              style={{
                                flex: 1,
                                color: 'rgb(71, 79, 88)',
                                textAlign: 'center',
                              }}
                              uppercase={false}
                            >
                              Close
                            </Text>
                            <Icon
                              style={{ flex: 1, textAlign: 'right', color: 'grey' }}
                              type="FontAwesome"
                              name="times"
                            />
                          </Button>
                        </View>
                        <SignPad submitSignature={this.submitSignature} />
                      </View>
                    </Modal>

                    {/* <View style={styles.modalHeader}>
                                                <Text style={{
                                                  flex: 1,
                                                  textAlign: 'center',
                                                  marginTop: 10,
                                                  marginBottom: 10
                                                    }} uppercase={false}>
                                                  Manager Signature
                                                </Text>
                                              </View>
                                              <SignPad submitSignature={this.submitSignature}/> */}
                  </View>
                </View>
              )}

            {contractDetails.contract.contract_status_id === 4 &&
              contractDetails.CURRENT_USER_IS_APPROVER === 0 && (
                <Card style={styles.card}>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: 'center',
                      marginTop: 30,
                      marginBottom: 30,
                      color: 'red',
                    }}
                    uppercase={false}
                  >
                    Not Approved
                  </Text>
                </Card>
              )}

            {contractDetails.contract.contract_status_id === 5 && (
              <Card style={styles.card}>
                <Text style={{ flex: 1, textAlign: 'left' }} uppercase={false}>
                  Employee Signature
                </Text>
                <Image
                  style={{
                    width: 350,
                    height: 150,
                    marginTop: 10,
                    borderWidth: 0.5,
                    borderColor: 'grey',
                  }}
                  source={{
                    uri: `${contractDetails.contract.signature_url}`,
                  }}
                />
                <Text style={{ flex: 1, textAlign: 'left' }} uppercase={false}>
                  Manager Signature
                </Text>

                {contractDetails.contract.WhoCanApprove.map(who => (
                  <View key={who.approver_name}>
                    <Image
                      style={{
                        width: 350,
                        height: 150,
                        marginTop: 10,
                        borderWidth: 0.5,
                        borderColor: 'grey',
                      }}
                      source={{
                        uri: `${who.approver_signature_location}`,
                      }}
                    />
                    <Text style={{ flex: 1, color: 'green', textAlign: 'left' }} uppercase={false}>
                      Approved: {who.approval_date}
                    </Text>
                    <Text style={{ flex: 1, textAlign: 'left' }} uppercase={false}>
                      Approver Name: {who.approver_name}
                    </Text>
                  </View>
                ))}
              </Card>
            )}

            {contractDetails.contract.contract_status_id === 3 && (
              <Button
                title="Ready to Sign"
                style={styles.signButton}
                onPress={this.showSignPad}
                light
              >
                <Text style={{ flex: 1, textAlign: 'left', color: 'white' }} uppercase={false}>
                  Ready to Sign
                </Text>
                <Icon
                  style={{ flex: 1, textAlign: 'right', color: 'white' }}
                  type="FontAwesome"
                  name="angle-right"
                />
              </Button>
            )}

            <Modal isVisible={this.state.isModalVisible && this.props.putStatus > 0}>
              <View style={styles.modalContent}>
                <Text style={{ fontSize: 28, marginBottom: 20, color: 'grey' }}>Thank You</Text>
                <Text style={{ fontSize: 16, marginBottom: 20, color: 'grey' }}>
                  We have submitted your signature.
                </Text>
                <View style={styles.modalContent}>
                  <Button style={{ height: 60, width: 110 }} onPress={this.toggleModal} success>
                    <Text style={{ fontSize: 20 }} uppercase={false}>
                      Confirm
                    </Text>
                  </Button>
                </View>
              </View>
            </Modal>

            <Modal
              isVisible={this.state.showSignPad && this.props.putStatus === 0}
              style={styles.bottomModal}
            >
              <View style={styles.modalContent}>
                <View style={styles.modalHeader}>
                  <Text
                    style={{
                      flex: 1,
                      fontSize: 23,
                      color: 'rgb(71, 79, 88)',
                      textAlign: 'center',
                    }}
                    uppercase={false}
                  >
                    Signature
                  </Text>
                  <Button onPress={this.showSignPad} style={styles.btnTransparent}>
                    <Text
                      style={{
                        flex: 1,
                        color: 'rgb(71, 79, 88)',
                        textAlign: 'center',
                      }}
                      uppercase={false}
                    >
                      Close
                    </Text>
                    <Icon
                      style={{ flex: 1, textAlign: 'right', color: 'grey' }}
                      type="FontAwesome"
                      name="times"
                    />
                  </Button>
                </View>
                <SignPad submitSignature={this.submitSignature} />
              </View>
            </Modal>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  contractDetails:
    state.contractDetails.contractDetails.WhoCanApprove && state.contractDetails.contractDetails,
  putStatus: state.contractDetails.signaturePutStatus.status,
});

const mapDispatchToProps = dispatch => ({
  getContractDetailsRequest: bindActionCreators(getContractDetailsRequest, dispatch),
  putSignatureRequest: bindActionCreators(putSignatureRequest, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signature);
