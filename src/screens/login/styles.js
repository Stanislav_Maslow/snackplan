import React, {Dimensions} from 'react-native';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default {
    container: {
        padding: 30,
        width: deviceWidth,
        height: deviceHeight,
        backgroundColor: '#FBFAFA',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    column: {
        flex: 1,
        flexShrink: 0,
        flexDirection: 'column',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-around',
        minWidth: 400,
        maxWidth: Math.round(deviceWidth * 0.5)
    },
    card: {
        flex: 1,
        paddingTop: 50,
        paddingBottom: 50,
        paddingLeft: 75,
        paddingRight: 75,
        flexShrink: 0,
        flexDirection: 'column',
        alignSelf: 'center',
        alignItems: 'center'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    forgot: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    image: {
        flex: 0.5,
        flexDirection: 'row',
        marginBottom: 20,
        alignSelf: 'center',
    },
    title: {
        marginTop: 5,
        marginBottom: 5,
        color: '#cccccc',
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 25,
        lineHeight: 25,
        fontWeight: 'normal',
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        marginTop: 5,
        marginBottom: 5,
        paddingLeft: 35
    },
    button: {
        marginTop: 30,
        marginBottom: 5,
        flex: 0.7,
        flexDirection: 'row',
        alignSelf: 'center',
        backgroundColor: '#0077FF',
        borderRadius: 5,
        height: 50,
        paddingLeft: 10,
        paddingRight: 10,
    },
    link: {
        marginTop: 0,
        marginBottom: 5,
        flex: 1,
        flexDirection: 'row',
        textAlign: 'left',
        color: '#cccccc',
        fontSize: 15,
        fontWeight: 'normal',
    },
    section: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        position: 'relative',
        marginBottom: 0,
        paddingBottom: 0,
    },
    icon: {
        padding: 10,
        position: 'absolute',
        color: '#0187ff'
    },
};
