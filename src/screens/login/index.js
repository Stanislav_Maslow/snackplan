import React, {Component} from 'react';
import {ActivityIndicator, View, StatusBar, TextInput, Image, AsyncStorage} from 'react-native';
import {Container, Button, H3, Text, Icon, Toast, Form, Card} from 'native-base';
import styles from './styles';
import {UserApi} from './../../services/api/user'
import IconFA from "react-native-vector-icons/FontAwesome";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getUserDataRequest } from './../../redux/user'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loading: false,
            showToast: false,
            validated: false
        };
    }

    async componentDidMount() {
        const token = await AsyncStorage.getItem('token');
        if (token !== null){    
            this.props.navigation.navigate('Dashboard');
        }
    }


    signIn = async () => {
        if (!this.state.username) {
            alert('Username cannot be blank');
            return;
        }

        if (!this.state.password) {
            alert('Password cannot be blank');
            return;
        }

        this.setState({loading: true});
        try {
            const response = await UserApi.login({
                username: this.state.username,
                password: this.state.password,
            });
            if(!response.result){
                alert('Wrong credentials! Please try again');
                this.setState({loading: false});
                return;
            }
            await AsyncStorage.setItem('token', response.token);
            this.props.getUserDataRequest();
            Toast.show({
                text: `Welcome ${response.result.first_name} ${response.result.last_name}!!!`,
                buttonText: "Close",
                duration: 60000
            });
            this.setState({loading: false});
            this.props.navigation.navigate('Dashboard');
        } catch (e) {
            console.error(e);
            Toast.show({
                text: "Wrong credentials! Please try again",
                buttonText: "Close",
                duration: 60000
            });
            this.setState({loading: false});

        }
    };

    loginBtn() {
        return <Button
            title="Sign In"
            style={styles.button}
            onPress={this.signIn.bind(this)}
            disabled={this.state.loading}
        >
            <Text style={{flex: 1, textAlign: 'left'}} uppercase={false}>Enter</Text>
            <Icon style={{flex: 1, textAlign: 'right'}} type="FontAwesome" name="angle-right"/>
        </Button>
    }

    loadingAnimation() {
        return <Button
            title="Sign In"
            style={styles.button}
            onPress={this.signIn.bind(this)}
            disabled={this.state.loading}
        >
            <ActivityIndicator style={{flex: 1}} size="small"/>
        </Button>

    }

    resetPassword() {
        this.props.navigation.navigate('ForgotPassword');
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar barStyle="dark-content"/>
                <Form style={styles.column}>
                    <View style={{flex: 0.5}}/>
                    <Card style={styles.card}>
                        <View style={styles.row}>
                            <Image style={styles.image} source={ require('../../../assets/logo.png') }
                                   resizeMode="contain"/>
                        </View>
                        <View style={styles.row}>
                            <H3 style={styles.title}>Login</H3>
                        </View>
                        <View style={styles.section}>
                            <IconFA active name="user-o" style={styles.icon} size={20}/>
                            <TextInput style={styles.input} placeholder="Username"
                                       placeholderTextColor="#CAE3FF"
                                       underlineColorAndroid="#0187ff"
                                       selectionColor="#0187ff"
                                       onChangeText={(username) => this.setState({username})}
                                       editable={!this.state.loading}/>
                        </View>
                        <View style={styles.section}>
                            <Icon active name="ios-lock-outline" style={styles.icon} size={19}/>

                            <TextInput style={styles.input} placeholder="Password" placeholderTextColor="#CAE3FF"
                                       underlineColorAndroid="#0187ff"
                                       selectionColor="#0187ff"
                                       onChangeText={(password) => this.setState({password})}
                                       secureTextEntry={true} editable={!this.state.loading} required="true"/>
                        </View>
                        <View style={styles.forgot}>
                            <Text style={styles.link} onPress={this.resetPassword.bind(this)}>Forgot
                                password?</Text>
                        </View>
                        <View style={styles.row}>
                            {this.state.loading ? this.loadingAnimation() : this.loginBtn()}
                        </View>
                    </Card>
                    <View style={{flex: 0.5}}/>
                </Form>
            </Container>



        );
    }
}

const mapStateToProps = state => ({
    user: state.user && state.user.userData && state.user.userData.user,
    tab: state.dashboardTab.tabName
})

const mapDispatchToProps = dispatch => ({
    getUserDataRequest: bindActionCreators(getUserDataRequest, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);