import React, { Dimensions, Platform } from 'react-native';

export default {
    container: {
        paddingTop: 50,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        flex: 1
    },
    header: {
        backgroundColor: '#FAFBFC', 
        height: 109
    },
    headerWrapper: {
        flex: 1, 
        flexDirection: 'row', 
        paddingTop: 20, 
        alignItems: 'center', 
        paddingLeft: 20, 
        justifyContent: 'space-between'
    },
    headerRow: {
        flexDirection: 'row', 
        alignItems: 'center'
    },
    sidebarToggle: {
        borderBottomWidth: 0, 
        backgroundColor: '#fff', 
        marginRight: 41, 
        paddingLeft: 0, 
        paddingRight: 0
    },
    sidebarToggleIcon: {
        color: 'rgb(0,119,255)'
    },
    iconSearch: {
        color: 'rgb(153,153,153)', 
        fontSize: 25, 
        marginRight: 20
    },
    searchLabel: {
        color: 'rgb(153,153,153)', 
        fontSize: 20, 
        paddingRight: 42
    },
    modalOverlay: {
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: 'rgba(236,241,246, 0.5)'
    },
    modalContainer: {
        height: 524, 
        width: 480, 
        backgroundColor: '#FFFFFF', 
        borderRadius: 3
    },
    modalHeader: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginBottom: 18
    },
    saveBtnModal: {
        backgroundColor: 'rgb(97,189,12)', 
        width: 180, 
        height: 60, 
        marginRight: 20
    },
    btnText: {
        marginLeft: 40
    },
    btnIcon: {
        color: '#fff', 
        fontSize: 24, 
        marginRight: 34
    },
    myAccountCard: {
        backgroundColor: '#F8F9FB', 
        paddingTop: 39, 
        paddingLeft: 10, 
        paddingRight: 10
    },
    myAccountContainer: {
        flexDirection: 'row', 
        height: 500
    },
    photoContainer: {
        flexDirection: 'column', 
        flex: 1, 
        justifyContent: 'flex-start'
    },
    avatarCol: {
        flex: 1, 
        paddingLeft: 10, 
        paddingRight: 10
    },
    accountInfo: {
        flexDirection: 'column', 
        flex: 2, 
        justifyContent: 'flex-start'
    },
    rowWithMargin: {
        flexDirection: 'row', 
        marginBottom: 20
    },
    fieldsContainer: {
        flex: 1, 
        paddingLeft: 10, 
        paddingRight: 10
    },
    dividerContainer: {
        flexDirection: 'row', 
        marginBottom: 10
    },
    listItem: {
        marginLeft: 0, 
        borderBottomWidth: 0
    },
    footer: {
        flex: 1, 
        flexDirection: 'column', 
        justifyContent: 'flex-end'
    },
    footerContainer: {
        flexDirection: 'row', 
        justifyContent: 'flex-end', 
        alignItems: 'flex-end'
    },
    cancelBtn: {
        backgroundColor: 'rgb(71,79,88)', 
        width: 180, 
        height: 60, 
        marginRight: 20
    },
    footerBtnIcon: {
        color: '#fff', 
        fontSize: 24, 
        marginRight: 34
    },
    saveBtn: {
        backgroundColor: 'rgb(97,189,12)', 
        width: 180, 
        height: 60, 
        marginRight: 20
    },
    modalTitle: {
        color: 'rgb(71,79,88)', 
        fontSize: 28, 
        paddingLeft: 50, 
        paddingTop: 24
    },
    modalInputLabel: {
        color: '#BDC1C4', 
        fontSize: 13, 
        paddingLeft: 50, 
        paddingRight: 50, 
        marginBottom: 11
    },
    inputModal: {
        marginLeft: 50, 
        marginRight: 50, 
        marginBottom: 29
    },
    modalFooter: {
        flex: 1, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginTop: 17
    },
    flexRow: {
        flexDirection: 'row'
    },
    tabDashboard: {
        color: 'rgb(71,79,88)', 
        fontSize: 14, 
        fontWeight: 'bold'
    },
    tabBadge: {
        color: 'rgb(0,119,255)', 
        fontSize: 14, 
        fontWeight: 'bold'
    },
    label: {
        fontSize: 13,
        color: '#B0B5B9',
        marginBottom: 10,
        fontWeight: 'bold'
    },
    avatar: {
        width: 220,
        height: 220
    },
    input: {
        backgroundColor: '#fff',
        borderRadius: 3,
        fontSize: 16,
        color: 'rgb(71,79,88)'
    },
    inputWrapper: {
        borderRadius: 3,
        borderWidth: 1,
        borderColor: 'rgb(238,238,238)',
    },
    title: {
        fontSize: 28,
        color: 'rgb(71,79,88)',
        marginBottom: 32,
        paddingLeft: 19
    },
    divider: {
        height: 1,
        backgroundColor: 'rgb(238,238,238)'
    },
    changeBtn: {
        backgroundColor: '#3392FF',
        height: 57,
        borderRadius: 4
    },
    checkboxLabel: {
        fontSize: 18,
        color: 'rgb(50,60,71)'
    },
    uploadText: {
        fontSize: 13,
        color: '#4E9EFD',
        textDecorationLine: 'underline'
    }
};