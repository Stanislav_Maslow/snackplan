import React, { Component } from 'react';
import { View, StatusBar, Image, TouchableOpacity, Modal } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ImagePicker } from 'expo'
import {
    Container,
    Button,
    Icon,
    Item,
    Input,
    Card,
    Text,
    Row,
    CheckBox,
    Body,
    ListItem,
    Header,
    Toast
} from 'native-base'
import IconEn from 'react-native-vector-icons/Entypo'

import { changeTab } from './../../redux/dashboardTabs'
import { getUserDataRequest } from './../../redux/user'
import {UserApi} from './../../services/api/user'

import styles from './styles'

const defaultAvatar = require("../../../assets/images/avatar.jpg")

class Settings extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email_activity: false,
            email_newsletter: false,
            modalVisible: false,
            first_name: '',
            last_name: '',
            email_address: '',
            cell_phone: '',
            user_id: '',
            password: '',
            passwordNew: '',
            passwordNewConfirm: ''
        }
        this.onChangeName = this.onChangeName.bind(this)
        this.onEmailChange = this.onEmailChange.bind(this)
        this.onChangeNumber = this.onChangeNumber.bind(this)
        this.onChangeNewPasswordConfirm = this.onChangeNewPasswordConfirm.bind(this)
        this.onChangeNewPasswordInput = this.onChangeNewPasswordInput.bind(this)
        this.onChangePasswordInput = this.onChangePasswordInput.bind(this)
        this.cancel = this.cancel.bind(this)
    }
    pickImage = async () => {
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        })
    }
    setModalVisible(visible) {
        this.setState({modalVisible: visible})
    }
    componentDidMount() {
        this.setState({
            user_id: this.props.user.user_id,
            email_activity: this.props.user.email_activity,
            email_newsletter: this.props.user.email_newsletter,
            full_name: `${this.props.user.first_name} ${this.props.user.last_name}`,
            email_address: this.props.user.email_address,
            cell_phone: this.props.user.cell_phone
        })
    }

    cancel() {
        this.setState({
            user_id: this.props.user.user_id,
            email_activity: this.props.user.email_activity,
            email_newsletter: this.props.user.email_newsletter,
            full_name: `${this.props.user.first_name} ${this.props.user.last_name}`,
            email_address: this.props.user.email_address,
            cell_phone: this.props.user.cell_phone
        })
    }

    onChangeName(value) {
        this.setState({full_name: value})
    }

    onEmailChange(value) {
        this.setState({email_address: value})
    }

    onChangeNumber(value) {
        this.setState({cell_phone: value})
    }

    onChangePasswordInput(value) {
        this.setState({password: value})
    }

    onChangeNewPasswordInput(value) {
        this.setState({passwordNew: value})
    }

    onChangeNewPasswordConfirm(value) {
        this.setState({passwordNewConfirm: value})
    }

    saveData = async () => {
        try {
            const response = await UserApi.update(this.state.user_id, {
                first_name: this.state.full_name.split(' ')[0] || '',
                last_name: this.state.full_name.split(' ')[1] || '',
                email_activity: +this.state.email_activity,
                email_newsletter: +this.state.email_newsletter,
                email_address: this.state.email_address,
                cell_phone: +this.state.cell_phone
                }
            );
            if(!response.status) {
                Toast.show({
                    text: response.message,
                    buttonText: "X",
                    duration: 10000,
                    type: 'danger'
                });
                return;
            }
            this.props.getUserDataRequest();
            Toast.show({
                text: `Success!!!`,
                buttonText: "X",
                duration: 10000,                
                type: "success"
            });
        } catch (e) {
            Toast.show({
                text: "Wrong credentials! Please try again",
                buttonText: "Close",
                duration: 60000
            });
        }
    }

    changePassword = async () => {
        try {
            const response = await UserApi.changePassword({
                email_address: this.state.email_address,
                username: this.props.user.username,
                password: this.state.password,
                password_new: this.state.passwordNew,
                password_new_confirm: this.state.passwordNewConfirm,
                require_old_password: 1
            }) 
            if(!response.status) {
                Toast.show({
                    text: response.message,
                    buttonText: "X",
                    duration: 10000,
                    type: 'danger'
                });
                return;
            }
            this.setState({
                modalVisible: false,
                password: '',
                passwordNew: '',
                passwordNewConfirm: ''
            })
            Toast.show({
                text: `Success!!!`,
                buttonText: "X",
                duration: 10000,                
                type: "success"
            });
        } catch (e) {
            Toast.show({
                text: "Wrong credentials! Please try again",
                buttonText: "Close",
                duration: 60000
            });
        }
    }

    goToMySignature() {
        this.props.changeTab('employee')
        this.props.navigation.navigate('Dashboard')
    }

    goToEmployeeSignature() {
        this.props.changeTab('my')
        this.props.navigation.navigate('Dashboard')
    }

    goToLatestSignature() {
        this.props.changeTab('latest');
        this.props.navigation.navigate('Dashboard');
    }

    render() {
        const { mySignatureLength, employeeSignatureLength } = this.props
        return (
            <Container>
                <Header style={styles.header}>
                    <View style={styles.headerWrapper}>
                        <View style={styles.headerRow}>
                            <Button style={styles.sidebarToggle} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                                <Icon name="ios-menu" style={styles.sidebarToggleIcon}/>
                            </Button>
                            <TouchableOpacity style={styles.flexRow}
                                onPress={() => this.goToMySignature()}>
                                <Text style={styles.tabDashboard}>Employee Signature</Text>
                                <Text style={styles.tabBadge}> +{employeeSignatureLength}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.flexRow}
                                onPress={() => this.goToEmployeeSignature()}>
                                <Text style={[styles.tabDashboard, {paddingLeft: 25}]}>My Signature</Text>
                                <Text style={[styles.tabBadge, {paddingRight: 25}]}> +{mySignatureLength}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.flexRow}
                                onPress={() => this.goToLatestSignature()}>
                                <Text style={styles.tabDashboard}>Latest Added</Text> 
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={styles.headerRow}>
                            <Icon name="ios-search" style={styles.iconSearch}/>
                            <Text style={styles.searchLabel}>Search</Text>
                        </TouchableOpacity>
                    </View>
                </Header>
                <View style={styles.container}>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={this.state.modalVisible}
                        transparent={true}
                        onRequestClose={() => {}}>
                        <View style={styles.modalOverlay}>
                            <View style={styles.modalContainer}>
                                <View style={styles.modalHeader}>
                                    <Text style={styles.modalTitle}>Change Password</Text>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setModalVisible(false);
                                        }}
                                        style={{padding: 17}}>
                                        <Icon name='ios-close' style={{color: '#636A71'}}/>
                                    </TouchableOpacity>
                                </View>
                                <Text style={styles.modalInputLabel}>OLD PASSWORD</Text>
                                <Item regular style={[styles.inputWrapper, styles.inputModal]}>
                                    <Input style={styles.input} secureTextEntry={true} value={this.state.password} onChangeText={this.onChangePasswordInput}/>
                                </Item>
                                <Text style={styles.modalInputLabel}>NEW PASSWORD</Text>
                                <Item regular style={[styles.inputWrapper, styles.inputModal]}>
                                    <Input style={styles.input} secureTextEntry={true} value={this.state.passwordNew} onChangeText={this.onChangeNewPasswordInput}/>
                                </Item>
                                <Text style={styles.modalInputLabel}>CONFIRM PASSWORD</Text>
                                <Item regular style={[styles.inputWrapper, styles.inputModal]}>
                                    <Input style={styles.input} secureTextEntry={true} value={this.state.passwordNewConfirm} onChangeText={this.onChangeNewPasswordConfirm}/>
                                </Item>
                                <View style={styles.modalFooter}>
                                    <Button iconRight style={styles.saveBtnModal} onPress={this.changePassword}>
                                        <Text style={styles.btnText}>Save</Text>
                                        <IconEn name="save" style={styles.btnIcon}/>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </Modal>
                    <StatusBar barStyle="dark-content"/>
                    <Text style={styles.title}>My Account</Text>
                    <View style={{height: 500}}>
                        <Card style={styles.myAccountCard}>
                            <View style={styles.myAccountContainer}>
                                <View style={styles.photoContainer}>
                                    <View style={[styles.flexRow, {marginBottom: 17}]}>
                                        <View style={styles.avatarCol}>
                                            <Text style={styles.label}>PROFILE PICTURE</Text>
                                            <Image source={defaultAvatar} style={styles.avatar}/>
                                        </View>
                                    </View>
                                    <View style={styles.flexRow}>
                                        <View style={styles.avatarCol}>
                                            <TouchableOpacity onPress={()=> this.pickImage()}>
                                                <Text style={styles.uploadText}>
                                                    Choose a photo
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.accountInfo}>
                                    <View style={styles.rowWithMargin}>
                                        <View style={styles.fieldsContainer}>
                                            <Text style={styles.label}>NAME</Text>
                                            <Item regular style={styles.inputWrapper}>
                                                <Input placeholder='Name'  value={this.state.full_name} style={styles.input} onChangeText={this.onChangeName}/>
                                            </Item>
                                        </View>
                                        <View style={styles.fieldsContainer}>
                                            <Text style={styles.label}>EMAIL</Text>
                                            <Item regular style={styles.inputWrapper}>
                                                <Input placeholder='Email' value={this.state.email_address} style={styles.input} onChangeText={this.onEmailChange}/>
                                            </Item>
                                        </View>
                                    </View>
                                    <View style={styles.rowWithMargin}>
                                        <View style={styles.fieldsContainer}>
                                            <Text style={styles.label}>CELL NUMBER</Text>
                                            <Item regular style={styles.inputWrapper}>
                                                <Input placeholder='Number' value={`${this.state.cell_phone}`} style={styles.input} onChangeText={this.onChangeNumber}/>
                                            </Item>
                                        </View>
                                        <View style={{flex: 1, paddingLeft: 5, paddingRight: 5}}></View>
                                    </View>
                                    <View style={styles.rowWithMargin}>
                                        <View style={styles.fieldsContainer}>
                                            <View style={styles.divider}></View>
                                        </View>
                                    </View>
                                    <View style={styles.rowWithMargin}>
                                        <View style={styles.fieldsContainer}>
                                            <Button iconLeft style={styles.changeBtn} onPress={() => this.setModalVisible(true)}>
                                                <Icon name="ios-lock-outline" style={{color: '#fff'}}/>
                                                <Text style={{color: '#fff', fontSize: 13}}>Change Password</Text>
                                            </Button>
                                        </View>
                                    </View>
                                    <View style={styles.dividerContainer}>
                                        <View style={styles.fieldsContainer}>
                                            <View style={styles.divider}></View>
                                        </View>
                                    </View>
                                    <View style={styles.dividerContainer}>
                                        <View style={styles.fieldsContainer}>
                                            <ListItem style={styles.listItem} 
                                                onPress={() => this.setState({email_activity: !this.state.email_activity})}>
                                                <CheckBox color='#6DC41C' 
                                                    checked={!!this.state.email_activity}
                                                    onPress={() => this.setState({email_activity: !this.state.email_activity})}/>
                                                <Body>
                                                    <Text style={styles.checkboxLabel}>Send me activity notifications</Text>
                                                </Body>
                                            </ListItem>
                                        </View>
                                    </View>
                                    <View style={styles.dividerContainer}>
                                        <View style={styles.fieldsContainer}>
                                            <View style={styles.divider}></View>
                                        </View>
                                    </View>
                                    <View style={styles.dividerContainer}>
                                        <View style={styles.fieldsContainer}>
                                            <ListItem style={styles.listItem}
                                                onPress={() => this.setState({email_newsletter: !this.state.email_newsletter})}>
                                                <CheckBox color='#6DC41C' 
                                                    checked={!!this.state.email_newsletter}
                                                    onPress={() => this.setState({email_newsletter: !this.state.email_newsletter})}/>
                                                <Body>
                                                    <Text style={styles.checkboxLabel}>Send me the Payplan newsletter</Text>
                                                </Body>
                                            </ListItem>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </Card>     
                    </View>
                    <View style={styles.footer}>
                        <View style={styles.footerContainer}>
                            <Button iconRight style={styles.cancelBtn} onPress={this.cancel}>
                                <Text style={{marginLeft: 40}}>Cancel</Text>
                                <Icon name="md-close" style={styles.footerBtnIcon}/>
                            </Button>
                            <Button iconRight style={styles.saveBtn} onPress={() => this.saveData()}>
                                <Text style={{marginLeft: 40}}>Save</Text>
                                <IconEn name="save" style={styles.footerBtnIcon}/>
                            </Button>
                        </View>
                    </View>
                </View>
            </Container>
        );
    }
}
  
const mapDispatchToProps = dispatch => ({
    changeTab: bindActionCreators(changeTab, dispatch),
    getUserDataRequest: bindActionCreators(getUserDataRequest, dispatch)    
})

const mapStateToProps = state => ({
    user: state.user && state.user.userData && state.user.userData.user,
    employeeSignatureLength: state.signatureEmployee.employeeSignature ? state.signatureEmployee.employeeSignature.length : 0,
    mySignatureLength: state.signatureMy.mySignature ? state.signatureMy.mySignature.length : 0,  
})

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
