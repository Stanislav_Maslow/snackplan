import React, {Component} from "react";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Image, TouchableOpacity, AsyncStorage } from "react-native"
import {
    Content,
    Text,
    List,
    ListItem,
    View,
    Container,
    Left,
    Button,
    Icon
} from "native-base"

import { getUserDataRequest } from './../../redux/user'
import { changeTab } from './../../redux/dashboardTabs'

import IconMa from 'react-native-vector-icons/MaterialIcons'
import IconEn from 'react-native-vector-icons/Entypo'

import styles from "./style"

const drawerImage = require("../../../assets/logo.png")
const defaultAvatar = require("../../../assets/avatar.png")

const routes = [
    {
        name: "Latest Added",
        route: "Dashboard",
        icon: props => <Icon name="ios-clock-outline" style={{...props}}/>,
        bg: "#00000"
    },
    {
        name: "Settings",
        route: "Settings",
        icon: props => <IconMa active name="settings" style={{...props}}/>,
        bg: "#00000"
    },
    {
        name: "Log Out",
        route: "Login",
        icon: props => <IconEn active name="log-out" style={{...props}}/>,
        bg: "#00000"
    }
]

class SideBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 'latest'
        }
    }
    componentDidMount() {
        this.props.getUserDataRequest()
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            activeTab: nextProps.tab
        })
    }

    changeRoute(data) {
        if (data.name === 'Log Out') {
            AsyncStorage.removeItem('token', res => this.props.navigation.navigate(data.route))
        } else if (data.name === 'Latest Added') {
            this.props.changeTab('latest')
            this.props.navigation.navigate('Dashboard')
        } else {
            this.props.navigation.navigate(data.route)
        }
    }
    checkActiveTab(tab) {
        const { state } = this.props.navigation
        const currentRouteKey = state.routes[state.index].key
        return this.state.activeTab === tab && currentRouteKey === 'Dashboard'
    }
    checkLatestRoute(route) {
        const { state } = this.props.navigation
        const currentRouteKey = state.routes[state.index].key
        return route === currentRouteKey && route === 'Dashboard' && this.state.activeTab === 'latest'
    }
    checkRouteNotDashboard(route) {
        const { state } = this.props.navigation
        const currentRouteKey = state.routes[state.index].key
        return route === currentRouteKey && route !== 'Dashboard'
    }
    goToEmployeeSignature() {
        this.props.changeTab('employee')
        this.props.navigation.navigate('Dashboard')
    }
    goToMySignature() {
        this.props.changeTab('my')
        this.props.navigation.navigate('Dashboard')
    }

    render() {
        const { user, employeeSignatureLength, mySignatureLength } = this.props
        return (
            <Container>
                <Content
                    bounces={false}
                    style={styles.content}
                >
                    <View style={styles.container}>
                        <Image square style={styles.drawerImage} source={drawerImage}/>
                        <Image square source={defaultAvatar} style={styles.avatar}/>
                        <Text style={styles.fullname}>
                            {user && user.first_name} {user && user.last_name}
                        </Text>
                        <TouchableOpacity style={this.checkActiveTab('employee') ? styles.btnActive : styles.btn}
                            onPress={() => this.goToEmployeeSignature()}>
                            <Text style={this.checkActiveTab('employee') ? styles.btnTextActive : styles.btnText}>Employee signature</Text>
                            <View style={this.checkActiveTab('employee') ? styles.badgeActive : styles.badge}>
                                <Text style={this.checkActiveTab('employee') ? styles.badgeTextActive : styles.badgeText}>{employeeSignatureLength}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.checkActiveTab('my') ? styles.btnActive : styles.btn}
                            onPress={() => this.goToMySignature()}>
                            <Text style={this.checkActiveTab('my') ? styles.btnTextActive : styles.btnText}>My signature</Text>
                            <View style={this.checkActiveTab('my') ? styles.badgeActive : styles.badge}>
                                <Text style={this.checkActiveTab('my') ? styles.badgeTextActive : styles.badgeText}>{mySignatureLength}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    
                    <List>
                        {
                            routes.map((data, key) => 
                                <ListItem
                                    button
                                    noBorder
                                    onPress={() => this.changeRoute(data)}
                                    style={styles.listItem}
                                    key={key}
                                >
                                    <Left style={{alignItems: 'center'}}>
                                        {
                                            (this.checkLatestRoute(data.route) || this.checkRouteNotDashboard(data.route)) &&
                                                <View style={styles.activeMark}></View>
                                        }
                                        {data.icon({
                                            color: this.checkLatestRoute(data.route) ? 'rgb(0,119,255)' 
                                                    : this.checkRouteNotDashboard(data.route) ? 'rgb(0,119,255)' 
                                                    : '#474F58', 
                                            fontSize: 24, 
                                            width: 24, 
                                            marginLeft: this.checkLatestRoute(data.route) ? 35 
                                                : this.checkRouteNotDashboard(data.route) ? 35 : 42
                                        })}

                                        <Text style={
                                            this.checkLatestRoute(data.route) ? styles.textActive 
                                                : this.checkRouteNotDashboard(data.route) ? styles.textActive 
                                                : styles.textDefault
                                        }>
                                            {data.name}
                                        </Text>
                                    </Left>
                                </ListItem>
                            )
                        }
                    </List>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user && state.user.userData && state.user.userData.user,
    tab: state.dashboardTab.tabName,
    employeeSignatureLength: state.signatureEmployee.employeeSignature ? state.signatureEmployee.employeeSignature.length : 0,
    mySignatureLength: state.signatureMy.mySignature ? state.signatureMy.mySignature.length : 0,    
})
  
const mapDispatchToProps = dispatch => ({
    getUserDataRequest: bindActionCreators(getUserDataRequest, dispatch),
    changeTab: bindActionCreators(changeTab, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(SideBar)
