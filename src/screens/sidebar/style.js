const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  content: {
    flex: 1, 
    backgroundColor: "#fff", 
    top: -1
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  drawerImage: {
    width: 115,
    height: 39,
    resizeMode: "contain",
    marginBottom: 35,
    marginTop: 58
  },
  textDefault: {
    fontSize: 14,
    marginLeft: 22,
    color: '#474F58'
  },
  textActive: {
    color: 'rgb(0,119,255)',
    fontSize: 14,
    marginLeft: 22
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  },
  avatar: {
    width: 100,
    borderRadius: 100,
    height: 100,
    marginBottom: 10
  },
  fullname: {
    fontSize: 18,
    color: 'rgb(50,60,71)'
  },
  btn: {
    padding: 20,
    borderRadius: 5,
    width: 250,
    height: 75,
    marginTop: 30,
    borderColor: 'rgb(233,233,233)',
    borderWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: 'rgb(233,233,233)',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2
  },
  badge: {
    width: 25, 
    height: 25,
    borderRadius: 100, 
    backgroundColor: 'rgb(0,119,255)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeText: {
    color: '#fff', 
    textAlign: 'center',
    fontSize: 13
  },
  btnText: {
    color: "black",
    width: 80
  },
  listItem: {
    height: 73, 
    marginTop: 17, 
    marginLeft: 0
  },
  activeMark: {
    height: 73, 
    width: 7, 
    backgroundColor: 'rgb(0,119,255)', 
    borderBottomRightRadius: 5, 
    borderTopRightRadius: 5
  },
  btnActive: {
    padding: 20,
    borderRadius: 5,
    width: 250,
    height: 75,
    marginTop: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'rgb(0,119,255)',
    marginRight: 10,
    marginLeft: 2
  },
  badgeActive: {
    width: 25, 
    height: 25,
    borderRadius: 100, 
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  badgeTextActive: {
    color: 'rgb(0,119,255)', 
    textAlign: 'center',
    fontSize: 13
  },
  btnTextActive: {
    color: 'white',
    width: 80
  },
};
