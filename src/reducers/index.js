import { combineReducers } from 'redux';
import { employeeSignatureReducer } from '../redux/signature';
import { mySignatureReducer } from '../redux/mySignature';
import { userDataReducer } from '../redux/user';
import { dashboardTabReducer } from '../redux/dashboardTabs';
import { contractDetailsReducer } from '../redux/contractDetails';
import { activeReducer } from '../redux/active';

const rootReducer = combineReducers({
  signatureEmployee: employeeSignatureReducer,
  active: activeReducer,
  signatureMy: mySignatureReducer,
  user: userDataReducer,
  dashboardTab: dashboardTabReducer,
  contractDetails: contractDetailsReducer,
});

export default rootReducer;
